<div class="communication-form">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
        'id'=>'order-repair-form',
        'type' => 'vertical',
        'htmlOptions' => ['class' => 'form', 'data-type' => 'ajax-form'],
    ]); ?>
    <?php if (Yii::app()->user->hasFlash('order-repair-success')): ?>
    <script>
        $('#messageModal').modal('show');
        setTimeout(function(){
            $('#messageModal').modal('hide');
        }, 4000);
    </script>
    <?php endif ?>

<form class="form">
    <div class="row">
        <div class="col-md-4 col-sm-6">
            <?= $form->textFieldGroup($model, 'name', [
                'widgetOptions'=>[
                    'htmlOptions'=>[
                        'class' => '',
                        'autocomplete' => 'off'
                    ]
                ]
            ]); ?>
        </div><!-- /.col-md-4 -->
        <div class="col-md-4 col-sm-6">
          <?= $form->textFieldGroup($model, 'email', [
            'widgetOptions'=>[
                'htmlOptions'=>[
                    'class' => '',
                    'autocomplete' => 'off'
                ]
            ]
        ]); ?>
    </div><!-- /.col-md-4 -->
    <div class="col-md-4 col-sm-6">
       <div class="form-group">
        <?= $form->labelEx($model, 'phone', ['class' => 'control-label']) ?>
        <?php $this->widget('CMaskedTextFieldPhone', [
            'model' => $model,
            'attribute' => 'phone',
            'mask' => '+7(999)999-99-99',
            'htmlOptions'=>[
                'class' => 'data-mask form-control',
                'data-mask' => 'phone',
                'placeholder' => Yii::t('MailModule.mail', 'Ваш телефон'),
                'autocomplete' => 'off'
            ]
        ]) ?>
        <?php echo $form->error($model, 'phone'); ?>
    </div>
</div><!-- /.col-md-4 -->
</div><!-- /.row -->

<div class="form-order-repair__bottom">
    <div class="form-order-repair__captcha">
        <div class="g-recaptcha" data-sitekey="<?= Yii::app()->params['key']; ?>">
        </div>
        <?= $form->error($model, 'verifyCode');?>
    </div>

</div>
<div class="communication-form__bottom">
   <div class="btn-order">
    <button type="submit" class="button" data-send="ajax">Оставить заявку</button>
</div>
<div class="terms__btn-order">
    Оставляя заявку вы соглашаетесь с <a target="_blank" href="test.html">Условиями обработки персональных данных</a>
</div>
</div><!-- /.communication-form__bottom -->
<?php $this->endWidget(); ?>

</div><!-- /.communication__form -->