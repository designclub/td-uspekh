<div class="stock-slide owl-carousel owl-theme">
    <?php foreach ($dataProvider->getData() as $data): ?>
        <div class="slide slide-1">
            <div class="stock-slide__image"> 
                <?= CHtml::image($data->image->getImageUrl(0, 0)); ?>
            </div>
        </div><!-- /.slide-1 -->
    <?php endforeach ?>
</div><!-- /.stock-slide -->
