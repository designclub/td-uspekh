<div class="first-win owl-carousel owl-theme">
  <?php foreach ($models as $key => $slide): ?>
    <div class="first-win__slide">
        <div class="img-bg">
            <picture class="js-load-img js-load-picture">
                  <source 
                    srcset="" 
                      data-img-big="<?= $slide->getImageUrlWebp(0, 0, false, null,'image'); ?>"
                      data-img-md="<?= $slide->getImageUrlWebp(0, 0, false, null,'image'); ?>"
                      data-img-sm="<?= $slide->getImageUrlWebp(0, 0, false, null,'image'); ?>"
                      data-img-xs="<?= $slide->getImageUrlWebp(400, 400, false, null,'image'); ?>"
                    type="image/webp">
                  <img 
                    src="" 
                    data-img-big="<?= $slide->getImageNewUrl(0, 0, false, null,'image'); ?>"
                      data-img-md="<?= $slide->getImageNewUrl(0, 0, false, null,'image'); ?>"
                      data-img-sm="<?= $slide->getImageNewUrl(0, 0, false, null,'image'); ?>"
                      data-img-xs="<?= $slide->getImageNewUrl(400, 400, false, null,'image'); ?>"
                    alt="">
            </picture>
            </div><!-- /.img-bg -->
            <div class="info">
                <div class="container">
                    <div class="top-title">
                        <?= $slide->name; ?>
                    </div>
                    <div class="title">
                        <?= $slide->name_short; ?>
                    </div>

                    <div class="under-title">
                        <?= $slide->description_short; ?>
                    </div>

                            <!--
                            <div class="text">
                                <?= $slide->description; ?>
                            </div> -->
                            

                            <!--
                          <?php /* if($slide->button_name):*/ ?>
                         
								<div class="btn-container">
									<a class="button" href="<?php /*($slide->button_link) ?: '#'; ?>"><?= $slide->button_name; */?></a>
								</div> 
							<?php /* else: */?>
                           
							<div class="btn-container">
                                <div class="btn-product">
                                    <?php /*$this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
		                               'id' => 13
		                            ]);*/ ?>
                                </div>
                           	</div> 

                                <div class="btn-partner">
                                    <?php /*$this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
		                               'id' => 14
		                           ]);*/ ?>
                                </div>
                            </div>
                            <?php /* endif; */?> -->



                        </div><!-- /.container -->
                    </div><!-- /.info -->
                </div><!-- /.first-win__slide -->
            <?php endforeach ?>
            </div><!-- /.first-win -->