<?php
/** @var Page $page */

if ($page->layout) {
    $this->layout = "//layouts/{$page->layout}";
}

$this->title = $page->meta_title;
$this->breadcrumbs = [
    Yii::t('HomepageModule.homepage', 'Pages'),
    $page->title
];
$this->description = !empty($page->meta_description) ? $page->meta_description : Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = !empty($page->meta_keywords) ? $page->meta_keywords : Yii::app()->getModule('yupe')->siteKeyWords
?>
<?php $this->widget('application.modules.slider.widgets.SliderWidget'); ?>

<!-- products -->
<div class="products">
    <div class="products-bg-img1"></div>
    <div class="products-bg-img2"></div>
    <div class="container">
       <?php $this->widget('application.modules.page.widgets.PageProductWidget', ['parent_id'=>8]); ?>

        <div class="benefits">
            <div class="benefits__title">
                <div class="title">
                    Собственное<br>
                    <span>производство</span>

                </div>
                <!--
                <div class="under-title">Мы делаем качественную продукцию с 1997 года</div>
            -->
            </div><!-- /.benefits__title -->

            <div class="benefits__container">
                <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="image">
                                <picture class="js-load-img js-load-picture">
                                      <source 
                                        srcset="" 
                                          data-img-big="<?= $page->getImageUrlWebp(0, 0, false, null,'image'); ?>"
                                          data-img-md="<?= $page->getImageUrlWebp(0, 0, false, null,'image'); ?>"
                                          data-img-sm="<?= $page->getImageUrlWebp(0, 0, false, null,'image'); ?>"
                                          data-img-xs="<?= $page->getImageUrlWebp(400, 400, false, null,'image'); ?>"
                                        type="image/webp">
                                      <img 
                                        src="" 
                                        data-img-big="<?= $page->getImageNewUrl(0, 0, false, null,'image'); ?>"
                                          data-img-md="<?= $page->getImageNewUrl(0, 0, false, null,'image'); ?>"
                                          data-img-sm="<?= $page->getImageNewUrl(0, 0, false, null,'image'); ?>"
                                          data-img-xs="<?= $page->getImageNewUrl(400, 400, false, null,'image'); ?>"
                                        alt="">
                                </picture>
                                <div class="bg_shadow">
                                </div>
                            </div>
                        </div>
                        <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                            'id' => 15
                        ]); ?>
                </div><!-- /.row -->
            </div><!-- /.benefits__container -->
        </div><!-- /.benefits -->
    </div><!-- /.container -->
</div><!-- /.products -->

<!-- <div class="content-home-page">
    <div class="container">
        <?php /* $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
            'id' => 16
        ]); */ ?>
    </div>
</div>/.content-home-page -->

<div class="bisness">
    <div class="bisness-bg-img1"></div>
    <div class="bisness-bg-img2"></div>
    <div class="container">
        <div class="row">
             <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                'id' => 17
            ]); ?>
        </div><!-- /.row -->

        <div class="stock">
            <?php $this->widget('application.modules.page.widgets.PageProductWidget', ['parent_id'=>14, 'view'=>'page-akzii-widget']); ?>
        </div><!-- /.stock -->

        <div class="communication">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="communication__title">
                        <div class="title">
                            Заполните форму<br>
                            <span>и мы свяжемся с Вами</span>
                        </div>
                        <div class="under-title">Мы ответим на все ваши вопросы. Дополнительное описание формы обратной связи. Текст будет заменен</div>
                    </div><!-- /.communication__title -->

                  <?php $this->widget('application.modules.mail.widgets.OrderRepairWidget', ['view'=>'order-widget']); ?>
                </div><!-- /.col-md-6 -->
                <div class="col-md-6 communication-text__text">
                    <div class="text">
                        Торговый дом
                        <span>Успех</span>
                    </div>
                </div><!-- /.col-md-6 communication-text__text -->
            </div><!-- /.row -->
        </div><!-- /.communication -->
    </div><!-- /.container -->
</div><!-- /.bisness -->

<div class="sertificats">
    <div class="container">
        <?php $this->widget('application.modules.page.widgets.PageProductWidget', ['parent_id'=>17, 'view'=>'page-sertificat-widget']); ?>
    </div><!-- /.container -->
</div><!-- /.sertificats -->
