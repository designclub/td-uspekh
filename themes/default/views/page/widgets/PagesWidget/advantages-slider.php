<div class="advantages-slider-header">
	<?= $parent->title; ?>
</div>
<div class="owl-advantages owl-carousel">
<?php foreach ($pages as $page): ?>
    <div class="advantages-item">
        <div class="advantages-item-img">
            <?= CHtml::image($page->getImageUrl(0,0,true,null)) ?>
        </div>
        <div class="advantage-item-description">
	        <div class="item-description-count">
	        	<span><?= ++$count; ?></span> / <?= count($pages); ?>
	        </div>
	         <div class="item-description-header">
	        	<?= $page->title; ?>
	        </div>
	        <div class="item-description-text">
	        	<?= $page->body; ?>
	        </div>
	        <div class="item-description-img">
	        	<?= CHtml::image($page->getImageBigUrl(0,0,true,null)) ?>
	        </div>
        </div>
    </div>
<?php endforeach ?>
</div>