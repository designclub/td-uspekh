<div class="sertificats-title">
    <div class="row">
        <div class="col-md-8">
            <div class="title">
                Декларации<br>
                <span>о соответствиии и протоколы о лабораторном испытании</span>
            </div>
            <div class="under-title">Мы соблюдаем все требования и нормы по обработке
            и хранению продукции. Убедитесь сами.</div>
        </div><!-- /.col-md-8 -->
        <div class="sertificats__allbtn">
            <a href="/sertifikaty" class="all-sertificats">Все сертификаты</a>
        </div>
    </div><!-- /.row -->
</div><!-- /.sertificats__title -->

<div class="sertificats-slide owl-carousel owl-theme">
    <?php foreach ($pages as $page): ?>
        <div class="sertificats-slide__item">
            <div class="image">
                <a href="<?= $page->getImageUrl(); ?>" data-lightbox="roadtrip">
                    <img data-src="<?=$page->getImageUrl(); ?>" alt="">
                </a>
            </div>
            <div class="text"><?= $page->body ?></div>
        </div><!-- /.sertificats-slide__item -->
    <?php endforeach ?>
</div><!-- /.sertificats__slide -->
