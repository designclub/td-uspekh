<div class="products-slide owl-carousel owl-theme">
    <?php foreach ($pages as $page): ?>
        <div class="products-slide__item">
            <div class="top-title">
                <?= $page->desc ?>
            </div>
            <div class="image">
                <a href="<?= Yii::app()->createUrl('/page/page/view', ['slug'=>$page->slug]); ?>">
                    <picture class="js-load-img js-load-picture">
                          <source 
                            srcset="" 
                              data-img-big="<?= $page->getImageUrlWebp(0, 0, false, null,'image'); ?>"
                              data-img-md="<?= $page->getImageUrlWebp(0, 0, false, null,'image'); ?>"
                              data-img-sm="<?= $page->getImageUrlWebp(0, 0, false, null,'image'); ?>"
                              data-img-xs="<?= $page->getImageUrlWebp(400, 400, false, null,'image'); ?>"
                            type="image/webp">
                          <img 
                            src="" 
                            data-img-big="<?= $page->getImageNewUrl(0, 0, false, null,'image'); ?>"
                              data-img-md="<?= $page->getImageNewUrl(0, 0, false, null,'image'); ?>"
                              data-img-sm="<?= $page->getImageNewUrl(0, 0, false, null,'image'); ?>"
                              data-img-xs="<?= $page->getImageNewUrl(400, 400, false, null,'image'); ?>"
                            alt="">
                    </picture>
                </a>
            </div>
            <div class="title-link">
                <?= CHtml::link($page->title, ['/page/page/view', 'slug'=>$page->slug]) ?>
            </div>
            <div class="packed">
                <?= $page->upakov ?>
            </div>
            <div class="text">
                <?= $page->body ?>
            </div>
            <div class="read-products">
                <a href="<?= Yii::app()->createUrl('/page/page/view', ['slug'=>$page->slug]); ?>" class="read-button"></a>
            </div>
        </div><!-- /.products-slide__item -->
    <?php endforeach ?>
</div><!-- /.products-slide -->