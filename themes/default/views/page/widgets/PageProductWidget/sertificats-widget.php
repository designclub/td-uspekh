<div class="sertificats">
	<div class="row">
	<?php foreach ($pages as $page): ?>
		<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
			<div class="sertificats-slide__item">
				<div class="image">
					<a href="<?= $page->getImageUrl(); ?>" data-lightbox="roadtrip">
						<?= CHtml::image($page->getImageUrl(0, 0, true, null, 'image')); ?>
					</a>
				</div>
				<div class="text"><?= $page->body ?></div>
			</div>
		</div>
	<?php endforeach ?>
</div>
</div>
