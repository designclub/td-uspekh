<div class="stock__title">
    <div class="title">
        Свежие предложения<br>
        <span>и акции компании</span>
    </div>
    <div class="under-title">У нас всегда есть интересные и актуальные предложения для Вас</div>
</div><!-- /.stock__title -->
<div class="stock-slide owl-carousel owl-theme">
    <?php foreach ($pages as $page): ?>
        <div class="slide slide-1">
            <div class="stock-slide__image">
                <picture class="js-load-img js-load-picture">
                      <source 
                        srcset="" 
                          data-img-big="<?= $page->getImageUrlWebp(0, 0, false, null,'image'); ?>"
                          data-img-md="<?= $page->getImageUrlWebp(0, 0, false, null,'image'); ?>"
                          data-img-sm="<?= $page->getImageUrlWebp(0, 0, false, null,'image'); ?>"
                          data-img-xs="<?= $page->getImageUrlWebp(400, 400, false, null,'image'); ?>"
                        type="image/webp">
                      <img 
                        src="" 
                        data-img-big="<?= $page->getImageNewUrl(0, 0, false, null,'image'); ?>"
                          data-img-md="<?= $page->getImageNewUrl(0, 0, false, null,'image'); ?>"
                          data-img-sm="<?= $page->getImageNewUrl(0, 0, false, null,'image'); ?>"
                          data-img-xs="<?= $page->getImageNewUrl(400, 400, false, null,'image'); ?>"
                        alt="">
                </picture>
            </div>
            <div class="stock-slide__undertitle">
                <?= $page->desc ?>
            </div>
            <div class="stock-slide__title"><?= $page->title ?></div>
            <div class="stock-slide__text">
                <?= $page->body ?>
            </div>
            <?= CHtml::link('Узнать больше', ['/page/page/view', 'slug'=>$page->slug], ['class'=>'stock-slide__link']); ?>
        </div><!-- /.slide-1 -->
    <?php endforeach ?>
</div><!-- /.stock-slide -->
