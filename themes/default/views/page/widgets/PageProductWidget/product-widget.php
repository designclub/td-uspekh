<?php foreach ($pages as $page): ?>
    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
        <div class="products__item">
            <div class="top-title">
                <?= $page->desc ?>
            </div>
            <div class="image">
                <a href="<?= Yii::app()->createUrl('/page/page/view', ['slug'=>$page->slug]); ?>">
                    <img data-src="<?= $page->getImageUrl(0, 0, true, null, 'image') ?>" alt="">
                </a>
            </div>
            <div class="title-link">
                <?= CHtml::link($page->title, ['/page/page/view', 'slug'=>$page->slug]) ?>
            </div>
            <div class="packed">
                <?= $page->upakov ?>
            </div>
            <div class="text">
                <?= $page->body ?>
            </div>
            <div class="read-products">
                <a href="<?= Yii::app()->createUrl('/page/page/view', ['slug'=>$page->slug]); ?>" class="read-button"></a>
            </div>
        </div><!-- /.products__item -->
    </div><!-- /.col-lg-3 col-md-4 col-sm-6 col-xs-12 -->
<?php endforeach ?>