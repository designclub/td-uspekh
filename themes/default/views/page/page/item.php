<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
	$this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<div class="single-products">
	<div class="container">
		<?php $this->widget(
			'bootstrap.widgets.TbBreadcrumbs',
			[
				'links' => $this->breadcrumbs,
			]
		);?>
	</div><!-- /.container -->

	<div class="productItem">
		<div class="container">
			<div class="productItem__title">
				<h1><?= $model->title; ?></h1>
				<span><?= $model->upakov; ?></span>
			</div><!-- /.productItem__title -->

			<div class="productItem__wrap">
				<div class="productItem-cnt">
					<div class="productItem-cnt__image">
						<div class="image">
							<a href="<?= $model->getImageBigUrl(); ?>" data-lightbox="roadtrip">
								<?= CHtml::image($model->getImageBigUrl()); ?>
							</a>
						</div>
					</div><!-- /.productItem-cnt__image -->

					<div class="productItem-cnt__text">
						<div class="productItem-text__title">
							<h2>Характеристики продукта</h2>
						</div>
						<div class="productItem-text__text">
							<?= $model->body; ?>
						</div>
						<div class="productItem-text__spec">
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="spec-item">
										<span class="name">Вес упаковки:</span>
										<span class="desc"><?= $model->mass; ?></span>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="spec-item">
										<span class="name">Срок хранения:</span>
										<span class="desc"><?= $model->age; ?></span>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="spec-item">
										<span class="name">Условия хранения:</span>
										<span class="desc"><?= $model->uslovie; ?></span>
									</div>
								</div>
							</div>
						</div>

						<div class="productItem-text__sertificat">
							<div class="productItem-text__title">
								<h2>Сертификат</h2>
							</div>
							<div class="row">
								<?= $model->sertificat; ?>

							</div>
						</div>

						<div class="productItem-text__btn">
							<div class="btn-container">
								<div class="btn-product">
									<a class="button" href="#" data-toggle="modal" data-target="#callbackModal">Заказать</a>
								</div>
								<div class="btn-partner">
									<a href="#" class="button" data-toggle="modal" data-target="#callbackModal">Нужна консультация</a>
								</div>
							</div>
						</div>
					</div><!-- /.productItem-cnt__text -->
				</div><!-- /.productItem-cnt -->
				<div class="productItem__desc">
					<div class="productItem-text__title">
						<h2>Описание продукта</h2>
					</div>

					<div class="productItem-text__text">
						<?= $model->desc2; ?>
					</div>
				</div><!-- /.productItem__desc -->
			</div><!-- /.productItem__wrap -->
		</div><!-- /.container -->
	</div><!-- /.productItem -->

	<!-- products -->
	<div class="products">
		<div class="container">
			<div class="products__title">
				<div class="title">
					Посмотрите<br>
					<span>все наши товары</span>
				</div>
				<div class="under-title">У нас всегда есть интересные и актуальные предложения для Вас</div>
			</div><!-- /.products__title -->
			<div class="row">
				<?php $this->widget('application.modules.page.widgets.PageProductWidget', ['parent_id'=>8, 'view'=>'product-widget']); ?>
			</div><!-- /.row -->
		</div><!-- /.container -->
	</div><!-- /.products -->

	<div class="communication-wrap">
		<div class="bg-img1"></div>
		<div class="bg-img2"></div>
		<div class="container">
			<div class="communication">
				<div class="row">
					<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
						<div class="communication__title">
							<div class="title">
								Заполните форму<br>
								<span>и мы свяжемся с Вами</span>
							</div>
							<div class="under-title">Мы ответим на все ваши вопросы. Дополнительное описание формы обратной связи. Текст будет заменен</div>
						</div><!-- /.communication__title -->
						<?php $this->widget('application.modules.mail.widgets.OrderRepairWidget', ['view'=>'order-widget']); ?>
					</div><!-- /.col-md-6 -->
					<div class="col-md-6 communication-text__text">
						<div class="text">
							Торговый дом
							<span>Успех</span>
						</div>
					</div><!-- /.col-md-6 communication-text__text -->
				</div><!-- /.row -->

			</div><!-- /.communication -->
		</div>
	</div><!-- /.container -->
</div><!-- /.single-products -->