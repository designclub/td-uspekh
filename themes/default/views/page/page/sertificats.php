<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
	$this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<div class="sertificats-entry">

	<div class="container">
		<?php $this->widget(
			'bootstrap.widgets.TbBreadcrumbs',
			[
				'links' => $this->breadcrumbs,
			]
		);?>
	</div><!-- /.container -->

	<div class="container">
		<div class="sertificats-title">
			<div class="row">
				<div class="col-md-8">
					<div class="title">
						Декларации<br>
                <span>о соответствиии и протоколы о лабораторном испытании</span>
					</div>
				</div><!-- /.col-md-8 -->
			</div><!-- /.row -->
		</div><!-- /.sertificats__title -->
		<?php $this->widget('application.modules.page.widgets.PageProductWidget', ['parent_id'=>17, 'view'=>'sertificats-widget']); ?>
	</div><!-- /.container -->
</div>


