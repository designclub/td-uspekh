<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<div class="single-products">
    <div class="container">
         <?php $this->widget(
                'bootstrap.widgets.TbBreadcrumbs',
                [
                    'links' => $this->breadcrumbs,
                ]
            );?>
    </div><!-- /.container -->

    <div class="container">
        <div class="page_iner">
            <div class="text">
                <h1><?= $model->title; ?></h1>
               <?= $model->body; ?>
            </div>
            <?php if ($model->id==2): ?>
                    <?php $this->widget('application.modules.gallery.widgets.GalleryWidget', ['galleryId'=>1,'view'=>'inner-slider']) ?> 
            <?php endif ?>
            <?php if ($model->id==7): ?>
                    <?php $this->widget('application.modules.gallery.widgets.GalleryWidget', ['galleryId'=>2,'view'=>'inner-slider']) ?> 
            <?php endif ?>
        </div>  
    </div>
</div><!-- /.single-products -->