<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<div class="single-products">

        <div class="container">
         <?php $this->widget(
                'bootstrap.widgets.TbBreadcrumbs',
                [
                    'links' => $this->breadcrumbs,
                ]
            );?>
    </div><!-- /.container -->
<!-- products -->
                <div class="products">
                    <div class="container">
                        <div class="products__title">
                            <div class="title">
                                Мы не храним овощи <br>
                                <span>в долгом ящике, выбирайте из лучшего!</span>
                            </div>
                            <div class="under-title">У нас всегда есть интересные и актуальные предложения для Вас</div>
                        </div><!-- /.products__title -->
                        <div class="row">
                            <?php $this->widget('application.modules.page.widgets.PageProductWidget', ['parent_id'=>8, 'view'=>'product-widget']); ?>
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </div><!-- /.products -->
                <div class="container">
                   <div class="peeled_vegetables">
                       <?php $this->widget('application.modules.page.widgets.PagesNewWidget', [
                                'id' => 22,
                                'view' => '_peeled'
                            ]); ?>
                   </div> 
                </div>

                <div class="container">
                    <div class="advantages-slider">
                         <?php $this->widget('application.modules.page.widgets.PagesWidget', ['parent_id'=>23, 'view'=>'advantages-slider']); ?>
                    </div>
                </div>

                <div class="container">
                    <div class="communication">

                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="communication__title">
                                    <div class="title">
                                        Заполните форму<br>
                                        <span>и мы свяжемся с Вами</span>
                                    </div>
                                    <div class="under-title">Мы ответим на все ваши вопросы. Дополнительное описание формы обратной связи. Текст будет заменен</div>
                                </div><!-- /.communication__title -->
                              <?php $this->widget('application.modules.mail.widgets.OrderRepairWidget', ['view'=>'order-widget']); ?>
                            </div><!-- /.col-md-6 -->
                            <div class="col-md-6 communication-text__text">
                                <div class="text">
                                    Торговый дом
                                    <span>Успех</span>
                                </div>
                            </div><!-- /.col-md-6 communication-text__text -->
                        </div><!-- /.row -->

                    </div><!-- /.communication -->
                </div><!-- /.container -->
            </div><!-- /.single-products -->