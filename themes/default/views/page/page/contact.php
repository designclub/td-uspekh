<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<div class="single-products">

        <div class="container">
         <?php $this->widget(
                'bootstrap.widgets.TbBreadcrumbs',
                [
                    'links' => $this->breadcrumbs,
                ]
            );?>
    </div><!-- /.container -->

    <div class="container">
      <div class="contact-box">
        <div class="contact-box__info">
            <h1><?= $model->title; ?></h1>
            <?= $model->body; ?>
        </div>
        <div class="contact-box__map">
            <iframe src="<?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', ['id' => 18]); ?>" frameborder="0"></iframe>

        </div>
    </div>
</div>

</div><!-- /.single-products -->