<div class="menu-fix">
    <div class="menu-fix__icon-menu">
        <div class="icon-bars">
            <div class="icon-bars__item"></div>
            <div class="icon-bars__item"></div>
            <div class="icon-bars__item"></div>
        </div>
    </div>
    <div class="menu-fix__icon-close"><div></div></div>
    <div class="menu-fix__box">
        <div class="menu-fix__box2">
            <div class="menu-fix__box3">
                <?php $this->widget(
                     'zii.widgets.CMenu',
                     [
                         'encodeLabel' => false,
                         'items' => $this->params['items'],
                         'htmlOptions' => [
                            'class' => '',
                         ],
                     ]
                 ); ?>
            </div>
        </div>
        <div class="menu-fix__bottom">
            <div class="items <?= (Yii::app()->language == "ru") ? 'items-ru' : 'items-en' ?>">
                <?php /*$this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                    'id' => 1
                ]);*/ ?>
            </div>
        </div>
    </div>
</div>

