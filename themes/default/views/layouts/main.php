<!DOCTYPE html>
<html lang="<?= Yii::app()->language; ?>">
<head>
    <?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::HEAD_START);?>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta http-equiv="Content-Language" content="ru-RU" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title><?= $this->title;?></title>
    <meta name="description" content="<?= $this->description;?>" />
    <meta name="keywords" content="<?= $this->keywords;?>" />
    <meta name="google-site-verification" content="HL2Z26hIYgZ0Ul0UcEnf_m4xevIUyJmIX7gUhL1ESMU" />

    <?php if ($this->canonical): ?>
        <link rel="canonical" href="<?= $this->canonical ?>" />
    <?php endif; ?>

    <style>
        <?php /*file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/css/optimized.css');*/ ?>
    </style>
    <?php
    Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/css/main.css');
    Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/owl-carousel/owl.carousel.min.js', CClientScript::POS_END);
    Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/lightbox.min.js', CClientScript::POS_END);
    Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/main.js', CClientScript::POS_END);
    ?>

    <script>
        var yupeTokenName = '<?= Yii::app()->getRequest()->csrfTokenName;?>';
        var yupeToken = '<?= Yii::app()->getRequest()->getCsrfToken();?>';
    </script>
    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

        <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
            'id' => 1
        ]); ?>

    <?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::HEAD_END);?>
</head>

<body>
    <script>
        function loadCSS(hf) {
            var ms=document.createElement("link");ms.rel="stylesheet";
            ms.href=hf;document.getElementsByTagName("head")[0].appendChild(ms);
        }
        loadCSS("<?= $this->mainAssets . '/css/owl-carousel/owl.carousel.css' ?>");
        loadCSS("<?= $this->mainAssets . '/css/owl-carousel/owl.theme.default.css' ?>");
        loadCSS("<?= $this->mainAssets . '/css/jquery.fancybox.min.css' ?>");
        // loadCSS("<?= $this->mainAssets . '/font/stylesheet.css' ?>");
        // loadCSS("<?= $this->mainAssets . '/css/lightbox.css' ?>");
    </script>

<?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::BODY_START);?>

<!-- container -->
<div class="wrapper">
        <div class="wrap1">
            <header class="header">
                <div class="top-header">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="branding">
                                    <a href="/">
                                        <!-- <?= CHtml::image($this->mainAssets . '/img/logo.png', ''); ?> -->
                                        <img data-src="<?=$this->mainAssets . '/img/logo.png'; ?>" alt="">
                                    </a>
                                </div><!-- /.branding -->
                            </div><!-- /.col-md-3 -->

                            <div class="col-md-5">
                                <div class="contacts">
                                    <div class="address">
                                        <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                                            'id' => 9
                                        ]); ?>
                                        <a data-fancybox data-type="iframe" data-src="<?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', ['id' => 18]); ?>" href="javascript:;">Показать на карте</a>
                                    </div>
                                    <div class="email">
                                        <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                                            'id' => 3
                                        ]); ?>
                                        <a data-toggle="modal" data-target="#writeModal" href="#" class="js-button">Написать нам</a>
                                    </div>
                                </div><!-- /.contacts -->
                            </div><!-- /.col-md-4 -->
                            <!-- Mobile menu -->
                              <?php if(Yii::app()->hasModule('menu')): ?>
                                <?php $this->widget('application.modules.menu.widgets.MenuWidget', [
                                    'view' => 'mobile',
                                    'name' => 'top-menu'
                                ]); ?>
                            <?php endif; ?>
                            <div class="col-md-auto">
                                <div class="phone-order">
                                    <div class="left-block">
                                        <div class="phone">

                                             <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                                                'id' => 4
                                            ]); ?>
                                        </div>
                                        <div class="time">
                                             <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                                                'id' => 5
                                            ]); ?>
                                        </div>
                                    </div>

                                    <div class="btn-phone-order">
                                          <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                                                'id' => 6
                                            ]); ?>
                                    </div>
                                </div><!-- /.phone-order -->
                            </div><!-- /.col-md-auto -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </div><!-- /.top-header -->

                <div class="top-menu">
                    <div class="container">

                        <?php if(Yii::app()->hasModule('menu')): ?>
                            <?php $this->widget('application.modules.menu.widgets.MenuWidget', [
                                'view' => 'main',
                                'name' => 'top-menu'
                            ]); ?>
                        <?php endif; ?>

                    </div><!-- /.container -->
                </div><!-- /.top-menu -->
            </header><!-- /.header -->


            <?= $this->decodeWidgets($content); ?>


        </div><!-- /.wrap1 -->

        <div class="wrap2">
            <?php $this->renderPartial('//layouts/_footer'); ?>
        </div><!-- /.wrap2 -->
    </div><!-- /.wrapper -->

    <?php $this->widget('application.modules.mail.widgets.CallbackModalWidget'); ?>
    <?php $this->widget('application.modules.mail.widgets.CallbackWriteModalWidget'); ?>

    <div id="messageModal" class="modal modal-my fade in" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div data-dismiss="modal" class="modal-close"><div></div></div>
                <div class="modal-my-heading">
                    <strong>Уведомление</strong>
                </div>
            </div>
            <div class="modal-success-message">
                Ваша заявка успешно отправлена
            </div>
        </div>
    </div>
</div>
    <!-- breadcrumbs
        <?php $this->widget(
            'bootstrap.widgets.TbBreadcrumbs',
            [
                'links' => $this->breadcrumbs,
            ]
        );?>-->
    </div>

    <!-- container end -->

    <script type="text/javascript">
        [].forEach.call(document.querySelectorAll('img[data-src]'), function(img) {
                img.setAttribute('src', img.getAttribute('data-src'));
                img.onload = function() {
                    img.removeAttribute('data-src');
                };
            });
    </script>
    
    <!-- Yandex.Metrika counter --> 
    <script type="text/javascript" >
        function metrica() {
            (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym"); ym(56635255, "init", { clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true }); 
         }
         setTimeout(metrica, 5000);
    </script>
    <noscript>
        <div>
            <img src="https://mc.yandex.ru/watch/56635255" style="position:absolute; left:-9999px;" alt="" />
        </div>
    </noscript>
    <!-- /Yandex.Metrika counter -->


<?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::BODY_END);?>
<?php $fancybox = $this->widget(
        'gallery.extensions.fancybox3.AlFancybox', [
            'target' => "[data-fancybox]",
            'lang'   => 'ru',
            'config' => [
                'animationEffect' => "fade",
            ],
        ]
    ); ?>
</body>
</html>
