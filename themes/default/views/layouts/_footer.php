<footer class="footer">
    <div class="container">
        <div class="footer-top">
            <div class="row">
                <div class="col-md-3 col-sm-2 col-xs-7 footer-logo__cnt order-1">
                    <div class="footer-logo">
                        <div class="footer-logo__logo">
                            <?= CHtml::image($this->mainAssets . '/img/logo.png', ''); ?>
                        </div>
                        <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                            'id' => 12
                        ]); ?>
                    </div><!-- /.footer-logo -->
                </div><!-- /.col-md-4 -->

                <div class="col-md-2 col-sm-2 col-xs-5 order-2">
                    <div class="footer-menu footer-menu-product">
                        <div class="title-menu">Карта сайта</div>
                        <?php if(Yii::app()->hasModule('menu')): ?>
                            <?php $this->widget('application.modules.menu.widgets.MenuWidget', [
                                'view' => 'footer',
                                'name' => 'footer-menu-1'
                            ]); ?>
                        <?php endif; ?>
                    </div><!-- /.footer-menu footer-menu-product -->
                </div><!-- /.col-md-4 -->
                <div class="col-md-2 col-sm-2 col-xs-5 order-4">
                    <div class="footer-menu footer-menu-info">
                        <div class="title-menu">Карта сайта</div>
                        <?php if(Yii::app()->hasModule('menu')): ?>
                            <?php $this->widget('application.modules.menu.widgets.MenuWidget', [
                                'view' => 'footer',
                                'name' => 'footer-menu-2'
                            ]); ?>
                        <?php endif; ?>
                    </div><!-- /.footer-menu footer-menu-info -->
                </div><!-- /.col-md-4 -->

                <div class="col-md-3 col-sm-3 col-xs-7 order-3">
                    <div class="footer-contact-adr">
                        <div class="title-contact">Контакты</div>
                        <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                            'id' => 9
                        ]); ?>
                        
                        <a href="#" class="maps_link" data-fancybox data-type="iframe" data-src="<?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', ['id' => 18]); ?>" href="javascript:;">Показать на карте</a>
                        <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                            'id' => 3
                        ]); ?>
                        <a data-toggle="modal" data-target="#writeModal" href="#" class="sign_link">Написать нам</a>
                    </div><!-- /.footer-contact1 -->
                </div>
                <div class="col-md-2 col-sm-3 col-xs-12 order-5">
                    <div class="footer-contact__phone">
                        <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                            'id' => 11
                        ]); ?>
                    </div>
                    <div class="footer-contact__soc">
                        <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                            'id' => 10
                        ]); ?>
                    </div>
                    <div class="footer-contact__callback">
                        <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                            'id' => 8
                        ]); ?>
                    </div>
                </div><!-- /.col-md-6 footer-contact -->
            </div><!-- /.row -->
        </div><!-- /.footer-top -->

        <div class="footer__bot">
            <div class="footer__copy">
                Дополнительная информация о публичной оферте, защите информации на сайте и тд.
                Любая дополнительная правовая информация.
            </div>
            <div class="footer__dc-logo">
                <a target="_blank" href="http://dc56.ru">
                    <?= CHtml::image($this->mainAssets . '/img/dc_logo.png', ''); ?>
                </a>
            </div>
        </div><!-- /.footer__bot -->
    </div><!-- /.container -->
</footer><!-- /.footer -->