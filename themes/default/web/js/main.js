(function( $ ){
    // Плагин лоадер
    var methods = {
        show : function(elem) {
            this
                .fadeIn(500);
                // .addClass('active')
        },
        hide : function(elem) {
            this
                .delay(100).fadeOut(500);
                // .removeClass('active')
        }
    };

    $.fn.loader = function( method ) {
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        }
    };
    // Плагин лоадер end
})( jQuery );

$(document).ready(function() {
    /**
     * ajax на формах
     */
    $('form[data-type="ajax-form"]').on('click', function(event) {
        var elem = event.target;
        var dataSend = elem.getAttribute('data-send');
        if (dataSend == 'ajax') {
            var
                button   = $(elem),
                form     = button.parents('form'),
                type     = form.attr('method'),
                formId   = form.attr('id');

            //$('.ajax-loading').loader('show');

            $.ajax({
                type: type,
                data:  form.serialize(), //formData,
                dataType: 'html',
                success: function(html) {
                    var newForm = $(html)
                        .find('#'+formId);

                    //$('.ajax-loading').loader('hide');

                    $('#'+formId).html(newForm.html());

                        if($('#'+formId).find('input[data-mask="phone"]').is('.data-mask')){
                            $('#'+formId).find('input[data-mask="phone"]')
                                .mask("+7(999)999-99-99", {
                                    'placeholder':'_',
                                    'completed':function() {
                                        //console.log("ok");
                                    }
                                });
                        }

                        $.getScript("https://www.google.com/recaptcha/api.js", function () {});
                    },
            });
            return false;
        }
    });

    //Mobile menu
    $('.menu-fix__icon-menu').on('click', function(){
        $('body').addClass('bodymenu');
        $(".menu-fix").addClass('active');
        $(".menu-fix__box").addClass('active');
        $('.menu-fix__icon-close').addClass('active');
     });

    $('.menu-fix').on('click', function(e){
        if($(e.target).hasClass('menu-fix') || $(e.target).hasClass('menu-fix__icon-close') || $(e.target).parents('.menu-fix__icon-close').length > 0 ){
            $(".menu-fix__icon-close").removeClass('active');
            $('body').removeClass('bodymenu');
            $(".menu-fix").removeClass('active');
            $(".menu-fix__box").removeClass('active');
            return false;
        }
     });

    //Действия при изменении окна
    $(window).resize(function () {
        var innerWidth = window.innerWidth;
        if(innerWidth > 1300){
            $('.menu-fix__icon-close').removeClass('active');
            $('.menu-fix__icon-menu').removeClass('no-active');
            $('body').removeClass('bodymenu');
            // $('html').removeClass('htmlmenu');
            $(".menu-fix").removeClass('active');
            $('.menu-fix__box').css({
                'display': '',
                'left': ''
            }).removeClass('active');
        }
    });

     // Показывать карту только тогда, когда пользователь докрутил до нее
    if ($('div').hasClass('reviews')){ 
         var reviews = $('.reviews');
         var reviewsTop = reviews.offset().top;

         $(window).bind('scroll', function(){
            var windowTop = $(this).scrollTop();
            if (windowTop > reviewsTop) {
                $('#map').html('<script>Скрипт гогл или яндекс карты</script>')
                $(window).unbind('scroll')
            }
         });
    };
});


$(function() {
    $(".first-win").owlCarousel();
});

$(".first-win").owlCarousel({
    nav:false, // Включение меню след. пред.
    loop:false, // Включение элементов отображения
    items:1, // Кол-во отображаемых блоков в слайдере
    loop:true, //Бесконечное зацикливание слайдера
    dots: true, //Отображение навигационных «точек»
    autoplay: false, //Автоматическое пролистывание
    autoplayTimeout: 4000, //Интервалы между пролистыванием элементов
    dotClass: "slide-dots",
    mergeFit: true,
});

$(function() {
    $(".products-slide").owlCarousel();
});

$(".products-slide").owlCarousel({
    nav:true, // Включение меню след. пред.
    loop:false, // Включение элементов отображения
    items:4, // Кол-во отображаемых блоков в слайдере
    margin: 15,
    loop:true, //Бесконечное зацикливание слайдера
    dots: false, //Отображение навигационных «точек»
    autoplay: false, //Автоматическое пролистывание
    autoplayTimeout: 4000, //Интервалы между пролистыванием элементов
    navContainerClass: "slide-nav",
    responsive:{
        0:{
            items:1,
            nav:false
        },
        500:{
            items:2,
            nav:false
        },
        991:{
            items:3,
            nav:false
        },
        1199:{
            items:4,
            nav:true
        },
    }
});

$(function() {
    $(".stock-slide").owlCarousel();
});

$(".stock-slide").owlCarousel({
    nav:true, // Включение меню след. пред.
    loop:false, // Включение элементов отображения
    items:2, // Кол-во отображаемых блоков в слайдере
    margin: 15,
    loop:true, //Бесконечное зацикливание слайдера
    dots: false, //Отображение навигационных «точек»
    autoplay: false, //Автоматическое пролистывание
    autoplayTimeout: 4000, //Интервалы между пролистыванием элементов
    navContainerClass: "slide-nav",
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:false
        },
        991:{
            items:2,
            nav:true
        },
    }
});

$(function() {
    $(".sertificats-slide").owlCarousel();
});

$(".sertificats-slide").owlCarousel({
    nav:true, // Включение меню след. пред.
    loop:false, // Включение элементов отображения
    items:5, // Кол-во отображаемых блоков в слайдере
    margin: 15,
    loop:true, //Бесконечное зацикливание слайдера
    dots: false, //Отображение навигационных «точек»
    autoplay: false, //Автоматическое пролистывание
    autoplayTimeout: 4000, //Интервалы между пролистыванием элементов
    navContainerClass: "slide-nav",
    responsiveClass:true,
    responsive:{
        0:{
            items:2,
            nav:false
        },
        768:{
            items:3,
            nav:true
        },
        991:{
            items:4,
            nav:true
        },
    }
});

$(".owl-advantages").owlCarousel({
    loop:true,
    autoHeight: false,
    autoplay: true,
    autoplayTimeout:5000,
    items: 1,
    margin:10,
    nav:true,
});

// включаем капчу при нажатии на кнопки и при скроле вниз
$(document).ready(function(){
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    var scrollPrev = 0;

    $(document).delegate('.js-button','click', function() {
        $.getScript("https://www.google.com/recaptcha/api.js", function () {});
        head.appendChild(script);

        $('.js-button').off('click');
    });

    // При скролле 1000 пунктов вниз активируем рекапчу
    $(window).bind('scroll', function(){
        var windowTop = $(this).scrollTop();
        if (windowTop > 1000) {
            $(window).off('scroll');
            $.getScript("https://www.google.com/recaptcha/api.js", function () {});
        }
    });

    // Действия при изменении окна 
    $(window).resize(function () {
        if($('img,picture').hasClass('js-load-img')){
            $('.js-load-img').each(function(){
                lazySlideImg($(this));
            });
        }
    });

    /*****************************************************************/
    /*    Pагрузка изображений в зависимости от разрешения экрана    */
    /*****************************************************************/
    if($('img,picture').hasClass('js-load-img')){
        $('.js-load-img').each(function(){
            lazySlideImg($(this));
        });
    }

    function lazySlideImg(img){
        var imgSrc;
        var flag = false;

        var source = false;
        var sourceSrc;
        if(img.hasClass('js-load-picture')){
            source = $(img.find('source'));
            img = $(img.find('img'));
        }

        var innerWidth = window.innerWidth;
        if(innerWidth > 1920){
            if(!img.hasClass('img-big')){
                img.addClass('img-big').removeClass('img-md img-sm img-xs');
                imgSrc = img.data('img-big');

                if(source){
                    source.addClass('img-big').removeClass('img-md img-sm img-xs');
                    sourceSrc = source.data('img-big');
                }

                flag = true;
            }
        } else if(innerWidth > 640){
            if(!img.hasClass('img-md')){
                img.addClass('img-md').removeClass('img-big img-sm img-xs');
                imgSrc = img.data('img-md');

                if(source){
                    source.addClass('img-md').removeClass('img-big img-sm img-xs');
                    sourceSrc = source.data('img-md');
                }

                flag = true;
            }
        } else if(innerWidth > 480){
            if(!img.hasClass('img-sm')){
                img.addClass('img-sm').removeClass('img-big img-md img-xs');
                imgSrc = img.data('img-sm');

                if(source){
                    source.addClass('img-sm').removeClass('img-big img-md img-xs');
                    sourceSrc = source.data('img-sm');
                }

                flag = true;
            }
        } else {
            if(!img.hasClass('img-xs')){
                img.addClass('img-xs').removeClass('img-big img-md img-sm');
                imgSrc = img.data('img-xs');

                if(source){
                    source.addClass('img-xs').removeClass('img-big img-md img-sm');
                    sourceSrc = source.data('img-xs');
                }

                flag = true;
            }
        }

        if(flag){
            img.attr('src', imgSrc);
            if(source){
                source.attr('srcset', sourceSrc);
            }
        }
    }
    /**********************************************/

    // ленивая загрузка изображений
    [].forEach.call(document.querySelectorAll('img[data-src]'), function(img) {
        img.setAttribute('src', img.getAttribute('data-src'));
        img.onload = function() {
            img.removeAttribute('data-src');
        };
    });

    [].forEach.call(document.querySelectorAll('source[data-webp]'), function(source) {
        source.setAttribute('srcset', source.getAttribute('data-webp'));
        source.removeAttribute('data-webp');
    });
});

