-- MySQL dump 10.13  Distrib 5.6.46, for Linux (x86_64)
--
-- Host: localhost    Database: uspech
-- ------------------------------------------------------
-- Server version	5.6.46

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `yupe_blog_blog`
--

DROP TABLE IF EXISTS `yupe_blog_blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_blog_blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `icon` varchar(250) NOT NULL DEFAULT '',
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `member_status` int(11) NOT NULL DEFAULT '1',
  `post_status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_blog_blog_slug_lang` (`slug`,`lang`),
  KEY `ix_yupe_blog_blog_create_user` (`create_user_id`),
  KEY `ix_yupe_blog_blog_update_user` (`update_user_id`),
  KEY `ix_yupe_blog_blog_status` (`status`),
  KEY `ix_yupe_blog_blog_type` (`type`),
  KEY `ix_yupe_blog_blog_create_date` (`create_time`),
  KEY `ix_yupe_blog_blog_update_date` (`update_time`),
  KEY `ix_yupe_blog_blog_lang` (`lang`),
  KEY `ix_yupe_blog_blog_slug` (`slug`),
  KEY `ix_yupe_blog_blog_category_id` (`category_id`),
  CONSTRAINT `fk_yupe_blog_blog_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_blog_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `yupe_user_user` (`id`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_blog_update_user` FOREIGN KEY (`update_user_id`) REFERENCES `yupe_user_user` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_blog_blog`
--

LOCK TABLES `yupe_blog_blog` WRITE;
/*!40000 ALTER TABLE `yupe_blog_blog` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_blog_blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_blog_post`
--

DROP TABLE IF EXISTS `yupe_blog_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_blog_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_id` int(11) NOT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `publish_time` int(11) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `title` varchar(250) NOT NULL,
  `quote` text,
  `content` text NOT NULL,
  `link` varchar(250) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '0',
  `comment_status` int(11) NOT NULL DEFAULT '1',
  `create_user_ip` varchar(20) NOT NULL,
  `access_type` int(11) NOT NULL DEFAULT '1',
  `meta_keywords` varchar(250) NOT NULL DEFAULT '',
  `meta_description` varchar(250) NOT NULL DEFAULT '',
  `image` varchar(300) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `meta_title` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_blog_post_lang_slug` (`slug`,`lang`),
  KEY `ix_yupe_blog_post_blog_id` (`blog_id`),
  KEY `ix_yupe_blog_post_create_user_id` (`create_user_id`),
  KEY `ix_yupe_blog_post_update_user_id` (`update_user_id`),
  KEY `ix_yupe_blog_post_status` (`status`),
  KEY `ix_yupe_blog_post_access_type` (`access_type`),
  KEY `ix_yupe_blog_post_comment_status` (`comment_status`),
  KEY `ix_yupe_blog_post_lang` (`lang`),
  KEY `ix_yupe_blog_post_slug` (`slug`),
  KEY `ix_yupe_blog_post_publish_date` (`publish_time`),
  KEY `ix_yupe_blog_post_category_id` (`category_id`),
  CONSTRAINT `fk_yupe_blog_post_blog` FOREIGN KEY (`blog_id`) REFERENCES `yupe_blog_blog` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_post_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_post_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_post_update_user` FOREIGN KEY (`update_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_blog_post`
--

LOCK TABLES `yupe_blog_post` WRITE;
/*!40000 ALTER TABLE `yupe_blog_post` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_blog_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_blog_post_to_tag`
--

DROP TABLE IF EXISTS `yupe_blog_post_to_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_blog_post_to_tag` (
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`post_id`,`tag_id`),
  KEY `ix_yupe_blog_post_to_tag_post_id` (`post_id`),
  KEY `ix_yupe_blog_post_to_tag_tag_id` (`tag_id`),
  CONSTRAINT `fk_yupe_blog_post_to_tag_post_id` FOREIGN KEY (`post_id`) REFERENCES `yupe_blog_post` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_post_to_tag_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `yupe_blog_tag` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_blog_post_to_tag`
--

LOCK TABLES `yupe_blog_post_to_tag` WRITE;
/*!40000 ALTER TABLE `yupe_blog_post_to_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_blog_post_to_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_blog_tag`
--

DROP TABLE IF EXISTS `yupe_blog_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_blog_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_blog_tag_tag_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_blog_tag`
--

LOCK TABLES `yupe_blog_tag` WRITE;
/*!40000 ALTER TABLE `yupe_blog_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_blog_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_blog_user_to_blog`
--

DROP TABLE IF EXISTS `yupe_blog_user_to_blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_blog_user_to_blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `role` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `note` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_blog_user_to_blog_blog_user_to_blog_u_b` (`user_id`,`blog_id`),
  KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_user_id` (`user_id`),
  KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_id` (`blog_id`),
  KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_status` (`status`),
  KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_role` (`role`),
  CONSTRAINT `fk_yupe_blog_user_to_blog_blog_user_to_blog_blog_id` FOREIGN KEY (`blog_id`) REFERENCES `yupe_blog_blog` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_user_to_blog_blog_user_to_blog_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_blog_user_to_blog`
--

LOCK TABLES `yupe_blog_user_to_blog` WRITE;
/*!40000 ALTER TABLE `yupe_blog_user_to_blog` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_blog_user_to_blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_category_category`
--

DROP TABLE IF EXISTS `yupe_category_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_category_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_category_category_alias_lang` (`slug`,`lang`),
  KEY `ix_yupe_category_category_parent_id` (`parent_id`),
  KEY `ix_yupe_category_category_status` (`status`),
  CONSTRAINT `fk_yupe_category_category_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_category_category`
--

LOCK TABLES `yupe_category_category` WRITE;
/*!40000 ALTER TABLE `yupe_category_category` DISABLE KEYS */;
INSERT INTO `yupe_category_category` VALUES (1,NULL,'bloki-na-glavnoy','ru','Блоки на главной',NULL,'','',1),(2,NULL,'bloki-v-shapke','ru','Блоки в шапке',NULL,'','',1),(3,NULL,'bloki-v-podvale','ru','Блоки в подвале',NULL,'','',1);
/*!40000 ALTER TABLE `yupe_category_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_comment_comment`
--

DROP TABLE IF EXISTS `yupe_comment_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_comment_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `model` varchar(100) NOT NULL,
  `model_id` int(11) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `text` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(20) DEFAULT NULL,
  `level` int(11) DEFAULT '0',
  `root` int(11) DEFAULT '0',
  `lft` int(11) DEFAULT '0',
  `rgt` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_comment_comment_status` (`status`),
  KEY `ix_yupe_comment_comment_model_model_id` (`model`,`model_id`),
  KEY `ix_yupe_comment_comment_model` (`model`),
  KEY `ix_yupe_comment_comment_model_id` (`model_id`),
  KEY `ix_yupe_comment_comment_user_id` (`user_id`),
  KEY `ix_yupe_comment_comment_parent_id` (`parent_id`),
  KEY `ix_yupe_comment_comment_level` (`level`),
  KEY `ix_yupe_comment_comment_root` (`root`),
  KEY `ix_yupe_comment_comment_lft` (`lft`),
  KEY `ix_yupe_comment_comment_rgt` (`rgt`),
  CONSTRAINT `fk_yupe_comment_comment_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `yupe_comment_comment` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_comment_comment_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_comment_comment`
--

LOCK TABLES `yupe_comment_comment` WRITE;
/*!40000 ALTER TABLE `yupe_comment_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_comment_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_contentblock_content_block`
--

DROP TABLE IF EXISTS `yupe_contentblock_content_block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_contentblock_content_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `code` varchar(100) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `content` text NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_contentblock_content_block_code` (`code`),
  KEY `ix_yupe_contentblock_content_block_type` (`type`),
  KEY `ix_yupe_contentblock_content_block_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_contentblock_content_block`
--

LOCK TABLES `yupe_contentblock_content_block` WRITE;
/*!40000 ALTER TABLE `yupe_contentblock_content_block` DISABLE KEYS */;
INSERT INTO `yupe_contentblock_content_block` VALUES (1,'Скрипты для аналитики','skripty-dlya-analitiki',3,'','',NULL,1),(3,'Почтовый ящик','pochtovyy-yashchik',1,'<span>tduspeh56@yandex.ru</span>','',NULL,1),(4,'Телефон','telefon',1,'+7 919 <span>-869-21-28</span>','',NULL,1),(5,'Время работы','vremya-raboty',1,'Пн-Пт с 9:00 до 18:00','',NULL,1),(6,'Кнопка - Заказать звонок','knopka-zakazat-zvonok',1,'<a data-toggle=\"modal\" data-target=\"#callbackModal\" href=\"#\" class=\"btn\">Заказать звонок</a>','',NULL,1),(8,'Заказать звонок в подвале','zakazat-zvonok-v-podvale',1,'<a class=\"but but-green\" data-toggle=\"modal\" data-target=\"#callbackModal\" href=\"#\">Заказать звонок</a>','',NULL,1),(9,'Адрес в шапке','adres-v-shapke',1,'<span>г. Оренбург, улица Шевченко 30</span>','',NULL,1),(10,'Мы в соц. сетях','my-v-soc-setyah',1,'<span>Мы в соц. сетях </span>\r\n<a href=\"test.html\"><i class=\"icon icon-instagram\"></i></a>\r\n<a href=\"test.html\"><i class=\"icon icon-vk\"></i></a>','',NULL,1),(11,'Телефон в подвале','telefon-v-podvale',1,'<a href=\"tel:+74957778899\">+7 919 869-21-28</a>','',NULL,1),(12,'ИНН и ОГРН','inn-i-ogrn',1,'<span>Торговый дом “Успех”</span>\r\n<span>ИНН 5610224106 </span>\r\n<span>ОГРН 1175658000671</span>\r\n<a href=\"#\">Полная карта партнеры</a>','',3,1),(13,'Кнопка на главный слайдер - Продукция компании','knopka-na-glavnyy-slayder-produkciya-kompanii',1,'<a class=\"button\" href=\"/tovary\">Продукция компании</a','',NULL,1),(14,'Кнопка на главный слайдер - Стать партнером','knopka-na-glavnyy-slayder-stat-partnerom',1,'<a href=\"#\" class=\"button\">Стать партнером</a>','',NULL,1),(15,'Главная - собственное производство','glavnaya-sobstvennoe-proizvodstvo',3,'<div class=\"col-md-6 col-sm-12 col-xs-12\">\r\n	<div class=\"image\"><img src=\"http://xn----gtbe3bjdjt.xn--p1ai/uploads/image/3a591629c118d016bf65180aaf6d68aa.jpg\"><br>\r\n		<div class=\"bg_shadow\">\r\n		</div>\r\n	</div>\r\n</div><!-- /.col-md-6 --><div class=\"col-md-6 col-sm-12 col-xs-12\">\r\n	<div class=\"info\">\r\n		<div class=\"row\">\r\n			<div class=\"col-md-4 col-sm-4 col-xs-6\">\r\n				<div class=\"number\">690\r\n				</div>\r\n				<div class=\"text_number\">Партнеров по всей России\r\n				</div>\r\n			</div>\r\n			<div class=\"col-md-4 col-sm-4 col-xs-6\">\r\n				<div class=\"number\">12750\r\n				</div>\r\n				<div class=\"text_number\">Тонн продукции выпущено за 2018 год\r\n				</div>\r\n			</div>\r\n			<div class=\"col-md-4 col-sm-4 col-xs-6\">\r\n				<div class=\"number\">14\r\n				</div>\r\n				<div class=\"text_number\">Квалифицированных сотрудников\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<!-- /.row -->\r\n		<div class=\"row\">\r\n			<div class=\"text\">\r\n			</div>\r\n		</div>\r\n		<!-- /.row -->\r\n		<div class=\"benefits__icon\">\r\n			<div class=\"row\">\r\n				<div class=\"col-md-4 col-sm-6 col-xs-6\">\r\n					<div class=\"icon\"><svg height=\"120px\" width=\"120px\" viewBox=\"0 -2 512 511\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"m360.820312 329.558594h-57.398437l7.550781-38.660156v-.003907l.003906-.03125c.054688-.269531.082032-.542969.113282-.8125.011718-.097656.03125-.1875.039062-.285156.03125-.375.039063-.746094.027344-1.117187 0-.109376-.019531-.21875-.023438-.328126-.019531-.265624-.035156-.53125-.074218-.792968-.019532-.132813-.050782-.269532-.078125-.402344-.042969-.234375-.085938-.472656-.144531-.699219-.007813-.023437-.007813-.046875-.015626-.070312-.289062-1.078125-3.03125-10.832031-10.046874-20.828125-10.074219-14.347656-23.820313-21.929688-39.753907-21.929688h-3.214843c-15.933594 0-29.679688 7.582032-39.75 21.929688-7.019532 9.996094-9.761719 19.75-10.050782 20.828125-.007812.023437-.007812.046875-.015625.070312-.058593.230469-.101562.464844-.144531.699219-.023438.136719-.058594.269531-.078125.40625-.039063.261719-.054687.527344-.070313.789062-.007812.109376-.023437.21875-.027343.328126-.011719.371093-.003907.746093.027343 1.117187.007813.097656.03125.191406.039063.285156.03125.273438.0625.542969.113281.816407l.007813.027343v.003907l7.546875 38.660156h-61.101563c-5.523437 0-10 4.476562-10 10v87.558594c0 5.523437 4.476563 10 10 10h22.113281v59.574218c0 5.523438 4.476563 10 10 10 5.523438 0 10-4.476562 10-10v-59.574218h121.105469v59.574218c0 5.523438 4.476563 10 10 10 5.519531 0 10-4.476562 10-10v-59.574218h23.300781c5.523438 0 10-4.476563 10-10v-87.558594c0-5.523438-4.476562-10-10-10zm-103.015624-65.960938h3.214843c12.226563 0 20.078125 7.933594 24.746094 15.351563h-52.707031c4.671875-7.421875 12.523437-15.351563 24.746094-15.351563zm-28 35.351563h59.214843l-5.976562 30.609375h-47.261719zm121.015624 118.167969h-186.519531v-26.046876h107.78125c5.523438 0 10-4.476562 10-10 0-5.519531-4.476562-10-10-10h-107.78125v-21.511718h186.519531zm0 0\"></path><path d=\"m483.460938 215.59375 25.367187-23.707031c2.023437-1.890625 3.171875-4.539063 3.171875-7.308594v-96.183594c0-5.523437-4.476562-10-10-10h-52.230469v-42.125c0-19.722656-16.046875-35.769531-35.773437-35.769531h-127.824219c-19.726563 0-35.773437 16.046875-35.773437 35.769531 0 5.523438 4.480468 10 10 10 5.523437 0 10-4.476562 10-10 0-8.695312 7.078124-15.769531 15.773437-15.769531h127.824219c8.695312 0 15.769531 7.074219 15.769531 15.769531v42.125h-48.914063c-5.523437 0-10 4.476563-10 10v96.183594c0 2.757813 1.132813 5.386719 3.136719 7.277344l25.140625 23.707031v230.863281h-18.277344c-5.523437 0-10 4.476563-10 10v40.265625c0 5.523438 4.476563 10 10 10h121.148438c5.523438 0 10-4.476562 10-10v-40.265625c0-5.523437-4.476562-10-10-10h-18.539062zm-23.21875-117.199219v19.597657c0 .585937-.476563 1.0625-1.0625 1.0625h-35.085938c-.585938 0-1.0625-.476563-1.0625-1.0625v-19.597657zm-69.390626 0h12.179688v19.597657c0 11.613281 9.449219 21.0625 21.0625 21.0625h35.085938c11.613281 0 21.0625-9.449219 21.0625-21.0625v-19.597657h11.757812v81.84375l-22.484375 21.011719h-56.414063l-22.25-20.984375zm101.148438 388.296875h-101.148438v-20.265625h101.148438zm-72.871094-40.261718v-225.175782h44.332032v18.945313h-16c-5.523438 0-10 4.476562-10 10 0 5.523437 4.476562 10 10 10h16v17.871093h-16c-5.523438 0-10 4.480469-10 10 0 5.523438 4.476562 10 10 10h16v148.355469h-44.332032zm0 0\"></path><path d=\"m131.148438 446.429688h-15.71875v-309.613282h75.925781l39.035156 63.640625v22.21875c0 5.519531 4.476563 10 10 10h38.042969c5.523437 0 10-4.480469 10-10v-22.21875l41.960937-68.40625c.953125-1.558593 1.476563-3.40625 1.476563-5.234375v-37.535156c0-5.523438-4.476563-10-10-10h-131.855469c-2.757813 0-5.394531 1.136719-7.28125 3.148438-1.890625 2.007812-2.867187 4.707031-2.699219 7.460937 0 .023437.003906.054687.003906.085937.339844 5.410157 4.925782 9.589844 10.382813 9.367188.269531-.011719.539063-.03125.804687-.0625h120.644532v17.539062h-286.0625v-17.539062h74.0625c.53125.0625 1.070312.085938 1.621094.0625 5.417968-.222656 9.652343-4.710938 9.59375-10.097656 0-.125-.003907-.257813-.007813-.375-.21875-5.359375-4.628906-9.589844-9.992187-9.589844h-85.277344c-5.523438 0-10 4.476562-10 10v37.539062c0 5.523438 4.476562 10 10 10h9.914062v309.609376h-15.722656c-5.523438 0-10 4.476562-10 10v40.261718c0 5.523438 4.476562 10 10 10h121.148438c5.523437 0 10-4.476562 10-10v-40.261718c0-5.523438-4.476563-10-10-10zm138.761718-254.023438c-.964844 1.574219-1.476562 3.382812-1.476562 5.230469v15.039062h-18.042969v-15.039062c0-1.847657-.507813-3.65625-1.476563-5.230469l-34.09375-55.585938h89.1875zm-224.1875 21.039062h16c5.519532 0 10-4.476562 10-10 0-5.523437-4.480468-10-10-10h-16v-17.875h16c5.519532 0 10-4.476562 10-10 0-5.519531-4.480468-10-10-10h-16v-18.75h49.707032v309.609376h-49.707032zm75.425782 273.246094h-101.148438v-20.265625h101.152344v20.265625zm0 0\"></path><path d=\"m306.710938 384.894531c1.578124 3.851563 5.558593 6.382813 9.71875 6.167969 4.117187-.214844 7.765624-3.003906 9.03125-6.929688 1.28125-3.976562-.117188-8.449218-3.433594-10.992187-3.367188-2.578125-8.109375-2.730469-11.636719-.382813-3.917969 2.605469-5.488281 7.800782-3.679687 12.136719zm0 0\"></path><path d=\"m136.960938 93.105469c1.65625 3.976562 5.753906 6.476562 10.050781 6.136719 4.109375-.324219 7.652343-3.261719 8.792969-7.214844 1.121093-3.898438-.308594-8.21875-3.523438-10.683594-3.375-2.578125-8.105469-2.710938-11.640625-.378906-3.945313 2.605468-5.460937 7.796875-3.679687 12.140625zm0 0\"></path></svg><br>\r\n					</div>\r\n					<div class=\"text\">Новейшее европейское оборудование.\r\n					</div>\r\n				</div>\r\n				<div class=\"col-md-4 col-sm-6 col-xs-6\">\r\n					<div class=\"icon\">\r\n						<!--?xml version=\"1.0\" encoding=\"iso-8859-1\"?-->\r\n						<!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\r\n						<svg height=\"120px\" width=\"120px\" version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" viewBox=\"0 0 512 512\" style=\"enable-background:new 0 0 512 512;\" xml:space=\"preserve\">\r\n						<g>\r\n						<g>\r\n						<path d=\"M472.208,201.712c9.271-9.037,12.544-22.3,8.544-34.613c-4.001-12.313-14.445-21.118-27.257-22.979l-112.03-16.279 c-2.199-0.319-4.1-1.7-5.084-3.694L286.28,22.632c-5.729-11.61-17.331-18.822-30.278-18.822c-12.947,0-24.549,7.212-30.278,18.822 l-50.101,101.516c-0.985,1.993-2.885,3.374-5.085,3.694L58.51,144.12c-12.812,1.861-23.255,10.666-27.257,22.979 c-4.002,12.313-0.728,25.576,8.544,34.613l81.065,79.019c1.591,1.552,2.318,3.787,1.942,5.978l-19.137,111.576 c-2.188,12.761,2.958,25.414,13.432,33.024c10.474,7.612,24.102,8.595,35.56,2.572l100.201-52.679 c1.968-1.035,4.317-1.035,6.286,0l100.202,52.679c4.984,2.62,10.377,3.915,15.744,3.914c6.97,0,13.896-2.184,19.813-6.487 c10.474-7.611,15.621-20.265,13.432-33.024l-19.137-111.576c-0.375-2.191,0.351-4.426,1.942-5.978L472.208,201.712z M362.579,291.276l19.137,111.578c0.64,3.734-1.665,5.863-2.686,6.604c-1.022,0.74-3.76,2.277-7.112,0.513l-100.202-52.679 c-4.919-2.585-10.315-3.879-15.712-3.879c-5.397,0-10.794,1.294-15.712,3.878l-100.201,52.678 c-3.354,1.763-6.091,0.228-7.112-0.513c-1.021-0.741-3.327-2.87-2.686-6.604l19.137-111.576 c1.879-10.955-1.75-22.127-9.711-29.886l-81.065-79.019c-2.713-2.646-2.099-5.723-1.708-6.923 c0.389-1.201,1.702-4.052,5.451-4.596l112.027-16.279c10.999-1.598,20.504-8.502,25.424-18.471l50.101-101.516 c1.677-3.397,4.793-3.764,6.056-3.764c1.261,0,4.377,0.366,6.055,3.764v0.001l50.101,101.516 c4.919,9.969,14.423,16.873,25.422,18.471l112.029,16.279c3.749,0.544,5.061,3.395,5.451,4.596 c0.39,1.201,1.005,4.279-1.709,6.923l-81.065,79.019C364.329,269.149,360.7,280.321,362.579,291.276z\"></path>\r\n						</g>\r\n						</g>\r\n						<g>\r\n						<g>\r\n						<path d=\"M413.783,22.625c-6.036-4.384-14.481-3.046-18.865,2.988l-14.337,19.732c-4.384,6.034-3.047,14.481,2.988,18.865 c2.399,1.741,5.176,2.58,7.928,2.58c4.177,0,8.295-1.931,10.937-5.567l14.337-19.732 C421.155,35.456,419.818,27.009,413.783,22.625z\"></path>\r\n						</g>\r\n						</g>\r\n						<g>\r\n						<g>\r\n						<path d=\"M131.36,45.265l-14.337-19.732c-4.383-6.032-12.829-7.37-18.865-2.988c-6.034,4.384-7.372,12.831-2.988,18.865 l14.337,19.732c2.643,3.639,6.761,5.569,10.939,5.569c2.753,0,5.531-0.839,7.927-2.581C134.407,59.747,135.745,51.3,131.36,45.265 z\"></path>\r\n						</g>\r\n						</g>\r\n						<g>\r\n						<g>\r\n						<path d=\"M49.552,306.829c-2.305-7.093-9.924-10.976-17.019-8.671l-23.197,7.538c-7.095,2.305-10.976,9.926-8.671,17.019 c1.854,5.709,7.149,9.337,12.842,9.337c1.383,0,2.79-0.215,4.177-0.666l23.197-7.538 C47.975,321.543,51.857,313.924,49.552,306.829z\"></path>\r\n						</g>\r\n						</g>\r\n						<g>\r\n						<g>\r\n						<path d=\"M256.005,456.786c-7.459,0-13.506,6.047-13.506,13.506v24.392c0,7.459,6.047,13.506,13.506,13.506 c7.459,0,13.506-6.047,13.506-13.506v-24.392C269.511,462.832,263.465,456.786,256.005,456.786z\"></path>\r\n						</g>\r\n						</g>\r\n						<g>\r\n						<g>\r\n						<path d=\"M502.664,305.715l-23.197-7.538c-7.092-2.303-14.714,1.577-17.019,8.672c-2.305,7.095,1.576,14.714,8.671,17.019 l23.197,7.538c1.387,0.45,2.793,0.664,4.176,0.664c5.694,0,10.989-3.629,12.843-9.337 C513.64,315.639,509.758,308.02,502.664,305.715z\"></path>\r\n						</g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						</svg>\r\n						<br>\r\n					</div>\r\n					<div class=\"text\">Приоритет - санитарный контроль.\r\n					</div>\r\n				</div>\r\n				<div class=\"col-md-4 col-sm-6 col-xs-6\">\r\n					<div class=\"icon\"><!--?xml version=\"1.0\" encoding=\"utf-8\"?-->\r\n<!-- Svg Vector Icons : http://www.onlinewebfonts.com/icon -->\r\n\r\n<svg height=\"120px\" width=\"120px\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" viewBox=\"0 0 1000 1000\" enable-background=\"new 0 0 1000 1000\" xml:space=\"preserve\">\r\n<metadata> Svg Vector Icons : <a href=\"http://www.onlinewebfonts.com/icon\">http://www.onlinewebfonts.com/icon</a> </metadata>\r\n<g><path d=\"M10,309.4l426.9-175.3l11.6,28.2L21.6,337.6L10,309.4z\"></path><path d=\"M850.5,337.6L423.6,162.3l11.6-28.2l426.9,175.3L850.5,337.6L850.5,337.6z\"></path><path d=\"M10,309.4h32.1v548.5H10V309.4z\"></path><path d=\"M830,309.4H862v161.8H830V309.4z\"></path><path d=\"M980.3,763.9H970c0,0-0.1,0-0.1,0c0-28.9,0-68.1,0-74.5c0-10.3-23.3-44.8-42.6-44.7c-19.3,0.1-31.9,0-31.9,0L854,518.5H707.1v277.2h75.1c4.2-32,31.5-56.7,64.6-56.7c33.1,0,60.4,24.7,64.6,56.7h58.5c0,0,0,0,0,0c0,0,0.1,0,0.1,0h10.3c5.3,0,9.7-4.3,9.7-9.7v-12.5C990,768.2,985.7,763.9,980.3,763.9z M823.1,625.8c0,4.6-3.8,8.4-8.4,8.4h-66.1c-4.6,0-8.4-3.8-8.4-8.4v-66.1c0-4.6,3.8-8.4,8.4-8.4h66.1c4.6,0,8.4,3.8,8.4,8.4V625.8z M846.8,748c-32.6,0-59,26.4-59,59c0,32.6,26.4,59,59,59s59-26.4,59-59C905.8,774.4,879.4,748,846.8,748z M846.8,843.9c-20.4,0-36.9-16.5-36.9-36.9s16.5-36.9,36.9-36.9s36.9,16.5,36.9,36.9S867.2,843.9,846.8,843.9z M279,748c-32.6,0-59,26.4-59,59c0,32.6,26.4,59,59,59c32.6,0,59-26.4,59-59C337.9,774.4,311.5,748,279,748z M279,843.9c-20.4,0-36.9-16.5-36.9-36.9S258.6,770,279,770c20.4,0,36.9,16.5,36.9,36.9C315.9,827.4,299.3,843.9,279,843.9z M421.1,748c-32.6,0-59,26.4-59,59c0,32.6,26.4,59,59,59c32.6,0,59-26.4,59-59C480.1,774.4,453.7,748,421.1,748z M421.1,843.9c-20.4,0-36.9-16.5-36.9-36.9s16.5-36.9,36.9-36.9c20.4,0,36.9,16.5,36.9,36.9C458.1,827.4,441.5,843.9,421.1,843.9z M115.7,795.7h98.6c4.2-32,31.5-56.7,64.6-56.7H115.7V795.7z M343.6,795.7h13c4.2-32,31.5-56.7,64.6-56.7H279C312.1,739,339.4,763.7,343.6,795.7z M657.4,760.1H545.9c-4.9,0-8.9,3.7-8.9,8.3v42.8c0,4.6,4,8.3,8.9,8.3h111.4c4.9,0,9-3.7,9-8.3v-42.8C666.3,763.9,662.3,760.1,657.4,760.1z M485.7,795.7h46.6V765c0-5.3,4.3-9.7,9.7-9.7h120.2c5.3,0,9.7,4.3,9.7,9.7v30.7h29.9V739H421.1C454.2,739,481.6,763.7,485.7,795.7z\"></path><path d=\"M478,712c0,1.8-1.5,3.3-3.3,3.3H353.3c-1.8,0-3.3-1.5-3.3-3.3v-51.7c0-1.8,1.5-3.3,3.3-3.3h121.4c1.8,0,3.3,1.5,3.3,3.3V712z\"></path><path d=\"M478,615.5c0,1.8-1.5,3.3-3.3,3.3H353.3c-1.8,0-3.3-1.5-3.3-3.3v-51.7c0-1.8,1.5-3.3,3.3-3.3h121.4c1.8,0,3.3,1.5,3.3,3.3V615.5z\"></path><path d=\"M477.7,487.3c0,0.9-1.5,1.6-3.3,1.6H353c-1.8,0-3.3-0.7-3.3-1.6v-25.8c0-0.9,1.5-1.6,3.3-1.6h121.4c1.8,0,3.3,0.7,3.3,1.6L477.7,487.3L477.7,487.3z\"></path><path d=\"M634.1,487.3c0,0.9-1.5,1.6-3.3,1.6H509.4c-1.8,0-3.3-0.7-3.3-1.6v-25.8c0-0.9,1.5-1.6,3.3-1.6h121.4c1.8,0,3.3,0.7,3.3,1.6V487.3z\"></path><path d=\"M636.1,615.5c0,1.8-1.5,3.3-3.3,3.3H511.4c-1.8,0-3.3-1.5-3.3-3.3v-51.7c0-1.8,1.5-3.3,3.3-3.3h121.4c1.8,0,3.3,1.5,3.3,3.3L636.1,615.5L636.1,615.5z\"></path><path d=\"M323.4,663.3c0,1.8-1.5,3.3-3.3,3.3H198.8c-1.8,0-3.3-1.5-3.3-3.3v-51.7c0-1.8,1.5-3.3,3.3-3.3h121.4c1.8,0,3.3,1.5,3.3,3.3V663.3z\"></path><path d=\"M323.4,573.4c0,1.8-1.5,3.3-3.3,3.3H198.8c-1.8,0-3.3-1.5-3.3-3.3v-51.7c0-1.8,1.5-3.3,3.3-3.3h121.4c1.8,0,3.3,1.5,3.3,3.3V573.4z\"></path><path d=\"M636.1,712c0,1.8-1.5,3.3-3.3,3.3H511.4c-1.8,0-3.3-1.5-3.3-3.3v-51.7c0-1.8,1.5-3.3,3.3-3.3h121.4c1.8,0,3.3,1.5,3.3,3.3L636.1,712L636.1,712z\"></path></g>\r\n</svg>\r\n						<br>\r\n					</div>\r\n					<div class=\"text\">Собственая складская база.\r\n					</div>\r\n				</div>\r\n				<div class=\"col-md-4 col-sm-6 col-xs-6\">\r\n					<div class=\"icon\"><svg height=\"120px\" width=\"120px\" viewBox=\"0 0 512.00002 512\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"m502 400.957031c-83.515625 0-165.484375 34.257813-224.890625 93.988281-3.898437 3.917969-3.878906 10.25.035156 14.140626 1.953125 1.941406 4.5 2.910156 7.054688 2.910156 2.566406 0 5.132812-.980469 7.089843-2.945313 55.679688-55.984375 132.480469-88.09375 210.710938-88.09375 5.523438 0 10-4.476562 10-10 0-5.519531-4.476562-10-10-10zm0 0\"></path><path d=\"m293.46875 391.695312c-3.09375-4.578124-9.308594-5.78125-13.882812-2.6875-42.761719 28.882813-78.875 65.023438-107.34375 107.414063-3.078126 4.585937-1.859376 10.800781 2.726562 13.878906 1.714844 1.148438 3.648438 1.699219 5.566406 1.699219 3.21875 0 6.382813-1.554688 8.3125-4.425781 27.023438-40.246094 61.316406-74.5625 101.933594-101.996094 4.574219-3.089844 5.777344-9.304687 2.6875-13.882813zm0 0\"></path><path d=\"m502 320.457031c-44.707031 0-88.957031 7.636719-131.519531 22.699219-5.207031 1.839844-7.933594 7.554688-6.089844 12.761719 1.449219 4.105469 5.308594 6.667969 9.425781 6.667969 1.105469 0 2.230469-.1875 3.335938-.578126 40.414062-14.300781 82.417968-21.550781 124.847656-21.550781 5.523438 0 10-4.476562 10-10 0-5.523437-4.476562-10-10-10zm0 0\"></path><path d=\"m143.277344 443.378906c65.789062-85.777344 161.023437-145.335937 266.109375-167.65625.019531-.003906.039062-.007812.054687-.011718 30.199219-6.410157 61.203125-9.75 92.558594-9.75 5.519531 0 10-4.480469 10-10 0-5.523438-4.480469-10-10-10-28.433594 0-56.757812 2.601562-84.601562 7.691406v-21.585938c36.144531-30.597656 55.984374-70.691406 55.984374-113.386718 0-44.144532-21.207031-85.507813-59.71875-116.472657-3.660156-2.941406-8.875-2.941406-12.535156 0-38.507812 30.964844-59.71875 72.328125-59.71875 116.472657 0 42.695312 19.84375 82.789062 55.984375 113.386718v25.675782c-19 4.324218-37.726562 9.835937-56.0625 16.472656v-58.550782c0-2.75-1.109375-5.238281-2.90625-7.046874v-.003907l-56-56.332031c-1.878906-1.886719-4.429687-2.949219-7.09375-2.949219h-114.332031c-2.660156 0-5.214844 1.0625-7.089844 2.949219l-56 56.332031v.003907c-1.796875 1.808593-2.910156 4.296874-2.910156 7.050781v38.1875c-6.96875-1.277344-13.96875-2.402344-21-3.363281v-35.648438c27.332031-23.511719 42.328125-54.175781 42.328125-86.84375 0-34.128906-16.359375-66.074219-46.0625-89.960938-3.660156-2.941406-8.871094-2.941406-12.53125 0-29.703125 23.882813-46.0625 55.832032-46.0625 89.960938 0 32.664062 14.996094 63.328125 42.328125 86.839844v33.351562c-14.582031-1.363281-29.261719-2.066406-44-2.066406-5.523438 0-10 4.476562-10 10s4.476562 10 10 10c79.960938 0 158.171875 21.15625 227.121094 61.292969-16.519532 11.226562-32.296875 23.488281-47.234375 36.6875-55.863281-28.460938-117.933594-43.480469-179.886719-43.480469-5.519531 0-10 4.476562-10 10s4.480469 10 10 10c56.371094 0 112.855469 13.109375 164.171875 38.003906-15.203125 14.707032-29.414063 30.441406-42.527344 47.074219-38.898437-16.3125-79.78125-24.578125-121.644531-24.578125-5.519531 0-10 4.476562-10 10s4.480469 10 10 10c37.5 0 74.164062 7.109375 109.1875 21.109375-6.910156 9.601563-13.476562 19.464844-19.652344 29.589844l-15.238281 24.964843c-2.878906 4.710938-1.386719 10.867188 3.324219 13.742188 1.628906.996094 3.425781 1.46875 5.203125 1.46875 3.367187 0 6.660156-1.707031 8.542969-4.792969l15.238281-24.964843c7.945312-13.019532 16.53125-25.546876 25.683593-37.574219.359376-.398438.695313-.824219.988282-1.289063zm218.136718-324.699218c0-35.542969 16.261719-69.210938 45.984376-95.59375 29.722656 26.382812 45.984374 60.050781 45.984374 95.59375 0 31.320312-12.640624 61.191406-35.984374 85.898437v-76.578125c0-5.523438-4.476563-10-10-10-5.523438 0-10 4.476562-10 10v76.578125c-23.347657-24.707031-35.984376-54.578125-35.984376-85.898437zm-329.742187 9.320312c0-25.507812 11.417969-49.734375 32.328125-68.984375 20.910156 19.246094 32.328125 43.476563 32.328125 68.984375 0 21.082031-7.808594 41.292969-22.328125 58.542969v-66.542969c0-5.523438-4.476562-10-10-10-5.519531 0-10 4.476562-10 10v66.542969c-14.519531-17.25-22.328125-37.460938-22.328125-58.542969zm275.621094 77.664062h-86.132813l-36.117187-36.332031h86.132812zm-146.292969-32.148437 31.960938 32.148437h-63.917969zm-46 52.148437h92v63.59375c-4.636719-2.132812-9.300781-4.195312-14-6.175781v-33.585937c0-5.519532-4.476562-10-10-10h-44c-5.519531 0-10 4.480468-10 10v11.890625c-4.648438-1.210938-9.3125-2.359375-14-3.429688zm58 49.527344c-7.929688-2.921875-15.929688-5.632812-24-8.121094v-7.574218h24zm54 23.851563v-73.378907h94.335938v56.3125c-17.128907 7.097657-33.867188 15.199219-50.09375 24.308594-5.1875 2.910156-10.3125 5.933594-15.378907 9.035156-9.453125-5.773437-19.078125-11.203124-28.863281-16.277343zm0 0\"></path><path d=\"m407.398438 100c2.628906 0 5.210937-1.070312 7.070312-2.933594 1.859375-1.859375 2.929688-4.4375 2.929688-7.066406 0-2.632812-1.066407-5.210938-2.929688-7.070312-1.859375-1.863282-4.441406-2.929688-7.070312-2.929688-2.628907 0-5.210938 1.066406-7.066407 2.929688-1.863281 1.859374-2.933593 4.4375-2.933593 7.070312 0 2.628906 1.070312 5.207031 2.933593 7.066406 1.855469 1.859375 4.4375 2.933594 7.066407 2.933594zm0 0\"></path><path d=\"m320.597656 365.269531c-1.855468 1.859375-2.929687 4.4375-2.929687 7.066407 0 2.640624 1.070312 5.210937 2.929687 7.070312 1.863282 1.863281 4.441406 2.929688 7.070313 2.929688 2.632812 0 5.210937-1.066407 7.070312-2.929688 1.859375-1.859375 2.929688-4.429688 2.929688-7.070312 0-2.628907-1.070313-5.207032-2.929688-7.066407-1.859375-1.863281-4.4375-2.933593-7.070312-2.933593-2.628907 0-5.207031 1.070312-7.070313 2.933593zm0 0\"></path></svg>\r\n						<br>\r\n					</div>\r\n					<div class=\"text\">Поставки напрямую из фермерских хозяйств.\r\n					</div>\r\n				</div>\r\n				<div class=\"col-md-4 col-sm-6 col-xs-6\">\r\n					<div class=\"icon\">\r\n						<!--?xml version=\"1.0\" encoding=\"iso-8859-1\"?-->\r\n						<!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\r\n						<svg height=\"120px\" width=\"120px\" version=\"1.1\" id=\"Capa_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" viewBox=\"0 0 512 512\" style=\"enable-background:new 0 0 512 512;\" xml:space=\"preserve\">\r\n						<g>\r\n						<g>\r\n						<path d=\"M181.72,366.814c-0.723-0.206-1.463-0.322-2.208-0.362c10.954-11.513,17.7-27.066,17.7-44.174v-52.611 c0-0.481-0.046-0.95-0.111-1.412c0.066-0.95,0.111-1.906,0.111-2.872v-33.286c0-5.523-4.477-10-10-10h-67.073 c-28.245,0-51.223,22.979-51.223,51.223v48.958c0,17.108,6.745,32.661,17.7,44.174c-0.744,0.04-1.484,0.156-2.206,0.362 C37.153,371.093,0,410.924,0,459.276V502c0,5.523,4.477,10,10,10h246.127c5.523,0,10-4.477,10-10v-42.724 C266.127,410.925,228.976,371.094,181.72,366.814z M88.916,286.001v-12.682c0-17.216,14.007-31.223,31.223-31.223h57.073v23.286 c0,11.723-9.537,21.26-21.26,21.26H88.916V286.001z M88.916,306.642h67.036c7.772,0,15.047-2.162,21.26-5.914v21.549 c0,24.343-19.805,44.148-44.148,44.148c-24.343,0-44.148-19.805-44.148-44.148V306.642z M154.829,386.426l-21.765,21.766 l-21.765-21.766H154.829z M246.128,492L246.128,492H20v-20h41.5c5.523,0,10-4.477,10-10s-4.477-10-10-10H20.365 c3.359-33.716,29.806-60.717,63.255-64.969l42.373,42.373c1.875,1.875,4.419,2.929,7.071,2.929c2.652,0,5.196-1.054,7.071-2.929 l42.373-42.373c35.824,4.553,63.62,35.204,63.62,72.245V492z\"></path>\r\n						</g>\r\n						</g>\r\n						<g>\r\n						<g>\r\n						<path d=\"M427.595,144.718c-0.723-0.206-1.464-0.322-2.21-0.362c10.954-11.513,17.699-27.066,17.699-44.174V47.57 c0-0.481-0.046-0.95-0.111-1.412c0.066-0.95,0.111-1.906,0.111-2.872V10c0-5.523-4.477-10-10-10h-67.073 c-28.245,0-51.223,22.979-51.223,51.223v48.958c0,17.108,6.745,32.661,17.699,44.174c-0.745,0.04-1.487,0.156-2.21,0.362 c-47.255,4.281-84.405,44.112-84.405,92.462v42.723c0,5.523,4.477,10,10,10h80.46c5.523,0,10-4.477,10-10s-4.477-10-10-10h-70.46 V237.18c0-37.041,27.795-67.692,63.619-72.245l42.373,42.373c1.953,1.953,4.512,2.929,7.071,2.929s5.119-0.976,7.071-2.929 l42.373-42.373C464.205,169.489,492,200.139,492,237.18v32.723h-73.667c-5.523,0-10,4.477-10,10s4.477,10,10,10H502 c5.523,0,10-4.477,10-10V237.18C512,188.83,474.849,148.999,427.595,144.718z M378.937,186.095l-21.765-21.765h43.53 L378.937,186.095z M423.085,100.181c0,24.343-19.805,44.148-44.148,44.148s-44.148-19.804-44.148-44.147V84.546h67.036 c7.772,0,15.047-2.163,21.26-5.914V100.181z M423.085,43.286c0,11.723-9.537,21.26-21.26,21.26h-67.036v-0.641V51.223 c0-17.216,14.007-31.223,31.223-31.223h57.073V43.286z\"></path>\r\n						</g>\r\n						</g>\r\n						<g>\r\n						<g>\r\n						<path d=\"M386.01,272.83c-1.86-1.86-4.44-2.93-7.07-2.93c-2.64,0-5.21,1.07-7.07,2.93c-1.87,1.86-2.93,4.44-2.93,7.07 c0,2.64,1.06,5.21,2.93,7.07c1.86,1.87,4.43,2.93,7.07,2.93c2.63,0,5.21-1.06,7.07-2.93c1.86-1.86,2.93-4.43,2.93-7.07 C388.94,277.27,387.87,274.69,386.01,272.83z\"></path>\r\n						</g>\r\n						</g>\r\n						<g>\r\n						<g>\r\n						<path d=\"M267.405,139.929l-41.821-41.821V50c0-27.57-22.43-50-50-50H50C22.43,0,0,22.43,0,50v88c0,27.57,22.43,50,50,50h125.583 c20.674,0,38.702-12.407,46.266-31h38.484c4.044,0,7.691-2.437,9.239-6.173C271.12,147.091,270.264,142.789,267.405,139.929z M214.554,137c-4.659,0-8.701,3.217-9.746,7.758C201.66,158.442,189.643,168,175.583,168H50c-16.542,0-30-13.458-30-30V50 c0-16.542,13.458-30,30-30h125.583c16.542,0,30,13.458,30,30v52.25c0,2.652,1.054,5.196,2.929,7.071L236.191,137H214.554z\"></path>\r\n						</g>\r\n						</g>\r\n						<g>\r\n						<g>\r\n						<path d=\"M462,324H336.417c-20.674,0-38.702,12.407-46.266,31h-38.484c-4.044,0-7.691,2.437-9.239,6.173 c-1.548,3.736-0.692,8.038,2.167,10.898l41.821,41.821V462c0,27.57,22.43,50,50,50H462c27.57,0,50-22.43,50-50v-88 C512,346.43,489.57,324,462,324z M492,462c0,16.542-13.458,30-30,30H336.417c-16.542,0-30-13.458-30-30v-52.25 c0-2.652-1.054-5.196-2.929-7.071L275.809,375h21.637c4.659,0,8.701-3.217,9.746-7.758C310.34,353.558,322.357,344,336.417,344 H462c16.542,0,30,13.458,30,30V462z\"></path>\r\n						</g>\r\n						</g>\r\n						<g>\r\n						<g>\r\n						<path d=\"M444.156,391.392c-3.904-3.905-10.236-3.905-14.141,0l-26.583,26.583l-12.361-12.361c-3.905-3.905-10.237-3.905-14.143,0 c-3.905,3.905-3.905,10.237,0,14.143l19.432,19.432c1.953,1.953,4.512,2.929,7.071,2.929s5.119-0.976,7.071-2.929l33.654-33.654 C448.061,401.63,448.061,395.298,444.156,391.392z\"></path>\r\n						</g>\r\n						</g>\r\n						<g>\r\n						<g>\r\n						<path d=\"M158.253,66.268c-1.182-17.147-15.004-30.969-32.151-32.151c-9.723-0.669-18.991,2.611-26.091,9.239 c-7.001,6.535-11.017,15.775-11.017,25.349c0.001,5.524,4.478,10.001,10.001,10.001s10-4.477,10-10 c0-4.111,1.656-7.921,4.664-10.729c3.003-2.804,6.938-4.196,11.069-3.906c7.239,0.499,13.074,6.334,13.573,13.573 c0.505,7.319-4.293,13.787-11.408,15.379c-7.788,1.742-13.227,8.513-13.227,16.465v5.335c0,5.523,4.477,10,10,10s10-4.477,10-10 v-2.91C149.16,97.291,159.385,82.684,158.253,66.268z\"></path>\r\n						</g>\r\n						</g>\r\n						<g>\r\n						<g>\r\n						<path d=\"M130.74,136.02c-1.86-1.86-4.44-2.93-7.07-2.93c-2.64,0-5.21,1.07-7.07,2.93c-1.87,1.86-2.93,4.44-2.93,7.07 s1.06,5.21,2.93,7.07c1.86,1.86,4.43,2.93,7.07,2.93c2.63,0,5.21-1.07,7.07-2.93c1.86-1.86,2.93-4.44,2.93-7.07 S132.6,137.88,130.74,136.02z\"></path>\r\n						</g>\r\n						</g>\r\n						<g>\r\n						<g>\r\n						<path d=\"M108.57,454.93c-1.86-1.86-4.44-2.93-7.07-2.93s-5.21,1.07-7.07,2.93s-2.93,4.44-2.93,7.07s1.07,5.21,2.93,7.07 c1.86,1.86,4.44,2.93,7.07,2.93s5.21-1.07,7.07-2.93s2.93-4.44,2.93-7.07S110.43,456.79,108.57,454.93z\"></path>\r\n						</g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						</svg>\r\n						<br>\r\n					</div>\r\n					<div class=\"text\">Предложение под требование клиента.\r\n					</div>\r\n				</div>\r\n				<div class=\"col-md-4 col-sm-6 col-xs-6\">\r\n					<div class=\"icon\">\r\n						<!--?xml version=\"1.0\" encoding=\"iso-8859-1\"?-->\r\n						<!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\r\n						<svg height=\"120px\" width=\"120px\" version=\"1.1\" id=\"Capa_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" viewBox=\"0 0 512 512\" style=\"enable-background:new 0 0 512 512;\" xml:space=\"preserve\">\r\n						<g>\r\n						<g>\r\n						<path d=\"M196,60c-5.52,0-10,4.48-10,10s4.48,10,10,10s10-4.48,10-10S201.52,60,196,60z\"></path>\r\n						</g>\r\n						</g>\r\n						<g>\r\n						<g>\r\n						<path d=\"M256,140c-5.514,0-10-4.486-10-10c0-5.514,4.486-10,10-10c3.542,0,7.282,1.807,10.815,5.227 c3.971,3.841,10.3,3.736,14.141-0.232c3.84-3.969,3.736-10.3-0.232-14.141c-5.075-4.911-10.153-7.689-14.724-9.205V90 c0-5.523-4.478-10-10-10c-5.522,0-10,4.477-10,10v11.72c-11.639,4.128-20,15.243-20,28.28c0,16.542,13.458,30,30,30 c5.514,0,10,4.486,10,10c0,5.514-4.486,10-10,10c-4.272,0-8.884-2.687-12.985-7.565c-3.553-4.228-9.862-4.773-14.089-1.219 c-4.228,3.554-4.773,9.862-1.22,14.089c5.346,6.359,11.632,10.79,18.294,13.024V210c0,5.523,4.478,10,10,10 c5.522,0,10-4.477,10-10v-11.72c11.639-4.128,20-15.243,20-28.28C286,153.458,272.542,140,256,140z\"></path>\r\n						</g>\r\n						</g>\r\n						<g>\r\n						<g>\r\n						<path d=\"M472,146c-22.056,0-40,17.944-40,40v71.27c-13.284-3.421-27.943,0.112-38.281,10.449l-67.065,67.065 C317.204,344.221,312,356.781,312,370.15V429h-10c-5.522,0-10,4.477-10,10v63c0,5.523,4.478,10,10,10h140c5.522,0,10-4.477,10-10 v-63c0-5.523-4.478-10-10-10h-10v-4.72c0-2.629,1.068-5.206,2.932-7.069l62.42-62.42C506.798,345.344,512,332.786,512,319.43V186 C512,163.944,494.056,146,472,146z M432,492H312v-43h120V492z M492,319.43c0,8.014-3.122,15.55-8.791,21.219l-62.42,62.42 c-5.586,5.586-8.789,13.317-8.789,21.211V429h-80v-58.85c0-8.022,3.12-15.556,8.791-21.219l67.07-67.07 c7.506-7.507,19.564-7.844,27.45-0.768c0.057,0.05,0.114,0.101,0.172,0.15c0.228,0.195,0.442,0.405,0.658,0.621 C439.919,285.639,442,290.659,442,296s-2.081,10.361-5.861,14.139l-49.69,49.69c-3.905,3.905-3.905,10.237,0,14.143 c3.906,3.905,10.236,3.906,14.143,0l49.688-49.687C457.837,316.731,462,306.687,462,296c0-9.84-3.538-19.132-10-26.44V186 c0-11.028,8.972-20,20-20s20,8.972,20,20V319.43z\"></path>\r\n						</g>\r\n						</g>\r\n						<g>\r\n						<g>\r\n						<path d=\"M210,429h-10v-58.85c0-13.369-5.204-25.929-14.648-35.361l-67.07-67.07C107.943,257.38,93.284,253.848,80,257.27V186 c0-22.056-17.944-40-40-40c-22.056,0-40,17.944-40,40v133.43c0,13.356,5.202,25.914,14.648,35.361l62.42,62.419 c1.864,1.864,2.932,4.44,2.932,7.07V429H70c-5.522,0-10,4.477-10,10v63c0,5.523,4.478,10,10,10h140c5.522,0,10-4.477,10-10v-63 C220,433.477,215.522,429,210,429z M28.791,340.65C23.122,334.98,20,327.444,20,319.43V186c0-11.028,8.972-20,20-20 s20,8.972,20,20v83.559c-6.462,7.308-10,16.599-10,26.441c0,10.687,4.163,20.731,11.719,28.281l50,50 c3.906,3.905,10.236,3.905,14.143,0c3.905-3.905,3.905-10.237,0-14.143l-50.003-50.003C72.081,306.361,70,301.341,70,296 s2.081-10.361,5.856-14.135c0.218-0.217,0.433-0.427,0.66-0.622l0.172-0.15c7.885-7.076,19.944-6.738,27.45,0.768l67.075,67.074 c5.666,5.659,8.786,13.192,8.786,21.214V429h-80v-4.72c0.001-7.894-3.202-15.625-8.788-21.211L28.791,340.65z M200,492H80v-43h120 V492z\"></path>\r\n						</g>\r\n						</g>\r\n						<g>\r\n						<g>\r\n						<path d=\"M256,0c-82.71,0-150,67.29-150,150s67.29,150,150,150s150-67.29,150-150S338.71,0,256,0z M256,280 c-71.683,0-130-58.318-130-130S184.317,20,256,20s130,58.318,130,130S327.683,280,256,280z\"></path>\r\n						</g>\r\n						</g>\r\n						<g>\r\n						<g>\r\n						<path d=\"M325.054,64.369c-26.021-21.01-60.077-28.934-93.421-21.745c-5.399,1.164-8.832,6.484-7.668,11.883 c1.164,5.398,6.485,8.828,11.883,7.667c27.409-5.91,55.345,0.561,76.643,17.755C333.786,97.124,346,122.663,346,150 c0,49.626-40.374,90-90,90c-49.626,0-90-40.374-90-90c0-14.824,3.68-29.518,10.642-42.492c2.611-4.866,0.783-10.928-4.083-13.54 c-4.867-2.612-10.93-0.783-13.54,4.083C150.502,113.924,146,131.887,146,150c0,60.654,49.346,110,110,110s110-49.346,110-110 C366,116.589,351.076,85.378,325.054,64.369z\"></path>\r\n						</g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						<g>\r\n						</g>\r\n						</svg>\r\n						<br>\r\n					</div>\r\n					<div class=\"text\">Работаем с отсрочкой платежа.\r\n					</div>\r\n				</div>\r\n			</div>\r\n			<!-- /.row -->\r\n		</div>\r\n		<!-- /.benefits__icon -->\r\n		<div class=\"btn-container\">\r\n			<div class=\"btn-product\">\r\n				<a class=\"button\" href=\"/proizvodstvo\">Узнать больше</a>\r\n			</div>\r\n			<div class=\"btn-partner\">\r\n				<a href=\"/tovary\" class=\"button\">Наша продукция</a>\r\n			</div>\r\n		</div>\r\n		<!-- /.btn-container -->\r\n	</div>\r\n	<!-- /.info -->\r\n</div><!-- /.col-md-6 -->','',1,1),(16,'Главная - блок черновик','glavnaya-blok-chernovik',3,'<div class=\"home-page__title\">\r\n	<div class=\"title\">\r\n		Блок<br>\r\n		<span>черновик</span>\r\n	</div>\r\n	<div class=\"under-title\">Подзаголовок блока с дополнительным описанием\r\n	</div>\r\n</div>\r\n<!-- /.home-page__title -->\r\n<div class=\"home-page__container\">\r\n	<div class=\"row\">\r\n		<div class=\"col-md-6 col-sm-12 col-xs-12\">\r\n			<div class=\"image\"><img src=\"http://uspech.rom/uploads/image/783aff91ac8b056003fd56daf164b6da.png\"><br>\r\n				<div class=\"bg_shadow\">\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<!-- /.col-md-6 -->\r\n		<div class=\"col-md-6 col-sm-12 col-xs-12\">\r\n			<div class=\"text\">\r\n				<p>Тестовый текст о предприятии, который в дальнейшем будет заменен вашим. Необходим для наглядности отображения информации на макете. Тестовый текст о предприятии, который в дальнейшем будет заменен вашим.\r\n				</p>\r\n				<p>Необходим для наглядности отображения информации на макете. Тестовый текст о предприятии, который в дальнейшем будет заменен вашим. Необходим для наглядности отображения информации на макете. Тестовый текст о предприятии, который в дальнейшем будет заменен вашим.\r\n				</p>\r\n			</div>\r\n			<!-- /.text -->\r\n			<div class=\"btn-container\">\r\n				<div class=\"btn-product\">\r\n					<a class=\"button\" href=\"test.html\">Узнать больше</a>\r\n				</div>\r\n				<div class=\"btn-partner\">\r\n					<a href=\"test.html\" class=\"button\">Наша продукция</a>\r\n				</div>\r\n			</div>\r\n			<!-- /.btn-container -->\r\n		</div>\r\n		<!-- /.col-md-6 -->\r\n	</div>\r\n	<!-- /.row -->\r\n</div>\r\n<!-- /.home-page__container -->','',1,1),(17,'Главная - для вашего бизнеса','glavnaya-dlya-vashego-biznesa',3,'<div class=\"bisness__col-left col-sm-12 col-xs-12\">\r\n	<div class=\"bisness__left\">\r\n		<div class=\"bisness__title\">\r\n			<div class=\"title\">\r\n				Предложение<br>\r\n				<span>для вашего бизнеса</span>\r\n			</div>\r\n			<div class=\"under-title\">\r\n			</div>\r\n		</div>\r\n		<div class=\"text\">\r\n			Мы будем рады сотрудничеству при любых объемах поставок и готовы предложить нашим будущим партнерам индивидуальные условия. Наши принципы в сотрудничестве: надежность поставок, своевременность выполнения обязательств, доверие к партнерам.\r\n		</div>\r\n		<div class=\"btn-container\">\r\n			<div class=\"btn-product\">\r\n				<a data-toggle=\"modal\" data-target=\"#callbackModal\" class=\"button\" href=\"#\">Оставить заявку</a>\r\n			</div>\r\n			<div class=\"desc__btn-product\">\r\n				Оставьте заявку и наши специалисты свяжутся с Вами для уточнения параметров заказа.\r\n			</div>\r\n		</div>\r\n	</div>\r\n</div>\r\n<div class=\"bisness__col-right col-sm-12 col-xs-12\">\r\n	<div class=\"bisness__right\">\r\n		<div class=\"row\">\r\n			<div class=\"col-md-4 bisness__right-block\">\r\n				<div class=\"block\">\r\n					<div class=\"block__title\">\r\n						Документы<br>и сертификаты\r\n					</div>\r\n					<div class=\"image\"><img src=\"/uploads/image/90b92faa30b62a1daa73a75aec6498fd.png\"><br>\r\n					</div>\r\n					<div class=\"bg_shadow\">\r\n					</div>\r\n					<a class=\"block__link\" href=\"/sertifikaty\"></a>\r\n				</div>\r\n				<!-- /.block -->\r\n				<div class=\"block\">\r\n					<div class=\"block__title\">\r\n						Продукция<br>компании\r\n					</div>\r\n					<div class=\"image\"><img src=\"/uploads/image/9b923efd98f5c07d282479c40e42ae9b.png\"><br>\r\n					</div>\r\n					<div class=\"bg_shadow\">\r\n					</div>\r\n					<a class=\"block__link\" href=\"/tovary\"></a>\r\n				</div>\r\n				<!-- /.block -->\r\n			</div>\r\n			<!-- /.col-md-4 -->\r\n			<div class=\"col-md-auto\">\r\n				<div class=\"bisness__right-image\">\r\n					<div class=\"text\">\r\n						Торговый дом\r\n                                    <span>Успех</span>\r\n					</div>\r\n				</div>\r\n				<div class=\"bg_shadow\">\r\n				</div>\r\n			</div>\r\n			<!-- /.col-md-8 -->\r\n		</div>\r\n		<!-- /.row -->\r\n	</div>\r\n	<!-- /.bisness__right -->\r\n</div>\r\n<!-- /.col-md-6 -->','',1,1),(18,'Карта яндекс','karta-yandeks',1,'https://yandex.ru/map-widget/v1/?um=constructor%3A2e06ba5fce0f9c32d45afb1404b24d9820910076d0d5e49cf44d4e0530060ab0&amp;source=constructor','',NULL,1);
/*!40000 ALTER TABLE `yupe_contentblock_content_block` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_feedback_feedback`
--

DROP TABLE IF EXISTS `yupe_feedback_feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_feedback_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `answer_user` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `phone` varchar(150) DEFAULT NULL,
  `theme` varchar(250) NOT NULL,
  `text` text NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `answer` text NOT NULL,
  `answer_time` datetime DEFAULT NULL,
  `is_faq` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_yupe_feedback_feedback_category` (`category_id`),
  KEY `ix_yupe_feedback_feedback_type` (`type`),
  KEY `ix_yupe_feedback_feedback_status` (`status`),
  KEY `ix_yupe_feedback_feedback_isfaq` (`is_faq`),
  KEY `ix_yupe_feedback_feedback_answer_user` (`answer_user`),
  CONSTRAINT `fk_yupe_feedback_feedback_answer_user` FOREIGN KEY (`answer_user`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_feedback_feedback_category` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_feedback_feedback`
--

LOCK TABLES `yupe_feedback_feedback` WRITE;
/*!40000 ALTER TABLE `yupe_feedback_feedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_feedback_feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_gallery_gallery`
--

DROP TABLE IF EXISTS `yupe_gallery_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_gallery_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `description` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `owner` int(11) DEFAULT NULL,
  `preview_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_gallery_gallery_status` (`status`),
  KEY `ix_yupe_gallery_gallery_owner` (`owner`),
  KEY `fk_yupe_gallery_gallery_gallery_preview_to_image` (`preview_id`),
  KEY `fk_yupe_gallery_gallery_gallery_to_category` (`category_id`),
  KEY `ix_yupe_gallery_gallery_sort` (`sort`),
  CONSTRAINT `fk_yupe_gallery_gallery_gallery_preview_to_image` FOREIGN KEY (`preview_id`) REFERENCES `yupe_image_image` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_gallery_gallery_gallery_to_category` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_gallery_gallery_owner` FOREIGN KEY (`owner`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_gallery_gallery`
--

LOCK TABLES `yupe_gallery_gallery` WRITE;
/*!40000 ALTER TABLE `yupe_gallery_gallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_gallery_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_gallery_image_to_gallery`
--

DROP TABLE IF EXISTS `yupe_gallery_image_to_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_gallery_image_to_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_id` int(11) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_gallery_image_to_gallery_gallery_to_image` (`image_id`,`gallery_id`),
  KEY `ix_yupe_gallery_image_to_gallery_gallery_to_image_image` (`image_id`),
  KEY `ix_yupe_gallery_image_to_gallery_gallery_to_image_gallery` (`gallery_id`),
  CONSTRAINT `fk_yupe_gallery_image_to_gallery_gallery_to_image_gallery` FOREIGN KEY (`gallery_id`) REFERENCES `yupe_gallery_gallery` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_gallery_image_to_gallery_gallery_to_image_image` FOREIGN KEY (`image_id`) REFERENCES `yupe_image_image` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_gallery_image_to_gallery`
--

LOCK TABLES `yupe_gallery_image_to_gallery` WRITE;
/*!40000 ALTER TABLE `yupe_gallery_image_to_gallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_gallery_image_to_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_image_image`
--

DROP TABLE IF EXISTS `yupe_image_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_image_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `file` varchar(250) NOT NULL,
  `create_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `alt` varchar(250) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_image_image_status` (`status`),
  KEY `ix_yupe_image_image_user` (`user_id`),
  KEY `ix_yupe_image_image_type` (`type`),
  KEY `ix_yupe_image_image_category_id` (`category_id`),
  KEY `fk_yupe_image_image_parent_id` (`parent_id`),
  CONSTRAINT `fk_yupe_image_image_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_image_image_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `yupe_image_image` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_image_image_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_image_image`
--

LOCK TABLES `yupe_image_image` WRITE;
/*!40000 ALTER TABLE `yupe_image_image` DISABLE KEYS */;
INSERT INTO `yupe_image_image` VALUES (1,NULL,NULL,'benefits-img.png','','b0a59606e8d436791ada6b4462767123.png','2019-08-17 18:04:28',1,'benefits-img.png',0,1,1),(2,NULL,NULL,'content-home-page-img.png','','783aff91ac8b056003fd56daf164b6da.png','2019-08-17 18:14:50',1,'content-home-page-img.png',0,1,2),(3,NULL,NULL,'bisness-block1.png','','90b92faa30b62a1daa73a75aec6498fd.png','2019-08-17 18:36:36',1,'bisness-block1.png',0,1,3),(4,NULL,NULL,'bisness-block2.png','','9b923efd98f5c07d282479c40e42ae9b.png','2019-08-17 18:36:56',1,'bisness-block2.png',0,1,4),(5,NULL,NULL,'benefits__icon1.png','','93c9e2227e4a406c9381f06d5d505d5b.png','2019-08-20 19:19:08',1,'benefits__icon1.png',0,1,5),(6,NULL,NULL,'benefits__icon2.png','','b654b3a3d7b20d29a13db3787ac55568.png','2019-08-20 19:19:39',1,'benefits__icon2.png',0,1,6),(7,NULL,NULL,'benefits__icon3.png','','d6eae9323812ce1e4521200e31cd870f.png','2019-08-20 19:20:12',1,'benefits__icon3.png',0,1,7),(8,NULL,NULL,'benefits__icon4.png','','0ba7a3e2f131d7f2aaa5d40f5fa5cbda.png','2019-08-20 19:20:34',1,'benefits__icon4.png',0,1,8),(9,NULL,NULL,'benefits__icon5.png','','869b359775f5f555faf7a28e400255d6.png','2019-08-20 19:20:55',1,'benefits__icon5.png',0,1,9),(10,NULL,NULL,'benefits__icon6.png','','60b333a16407b84d13c7a0febc406222.png','2019-08-20 19:21:22',1,'benefits__icon6.png',0,1,10),(11,NULL,NULL,'b0a59606e8d436791ada6b4462767123.jpg','','3a591629c118d016bf65180aaf6d68aa.jpg','2019-09-17 21:43:21',1,'b0a59606e8d436791ada6b4462767123.jpg',0,1,11);
/*!40000 ALTER TABLE `yupe_image_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_mail_mail_event`
--

DROP TABLE IF EXISTS `yupe_mail_mail_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_mail_mail_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(150) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_mail_mail_event_code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_mail_mail_event`
--

LOCK TABLES `yupe_mail_mail_event` WRITE;
/*!40000 ALTER TABLE `yupe_mail_mail_event` DISABLE KEYS */;
INSERT INTO `yupe_mail_mail_event` VALUES (1,'zakazat-zvonok','Заказать звонок',''),(2,'obratnyy-zvonok','Обратный звонок',''),(3,'napisat-nam','Написать нам','');
/*!40000 ALTER TABLE `yupe_mail_mail_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_mail_mail_template`
--

DROP TABLE IF EXISTS `yupe_mail_mail_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_mail_mail_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(150) NOT NULL,
  `event_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text,
  `from` varchar(150) NOT NULL,
  `to` varchar(150) NOT NULL,
  `theme` text NOT NULL,
  `body` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_mail_mail_template_code` (`code`),
  KEY `ix_yupe_mail_mail_template_status` (`status`),
  KEY `ix_yupe_mail_mail_template_event_id` (`event_id`),
  CONSTRAINT `fk_yupe_mail_mail_template_event_id` FOREIGN KEY (`event_id`) REFERENCES `yupe_mail_mail_event` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_mail_mail_template`
--

LOCK TABLES `yupe_mail_mail_template` WRITE;
/*!40000 ALTER TABLE `yupe_mail_mail_template` DISABLE KEYS */;
INSERT INTO `yupe_mail_mail_template` VALUES (1,'zakazat-zvonok',1,'Заказать звонок','','replay@td-uspekh.rf','tduspeh56@yandex.ru','Заявка - заказать звонок с сайта Успех','<table>\r\n<tbody>\r\n<tr>\r\n	<td>Телефон\r\n	</td>\r\n	<td>phone\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Имя\r\n	</td>\r\n	<td>name\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Сообщение</td>\r\n	<td>body</td>\r\n</tr>\r\n</tbody>\r\n</table>',1),(2,'obratnyy-zvonok',2,'Обратный звонок','','replay@td-uspekh.rf','tduspeh56@yandex.ru','Обратный звонок','<table>\r\n<tbody>\r\n<tr>\r\n	<td>Имя</td>\r\n	<td>name</td>\r\n</tr>\r\n<tr>\r\n	<td>Телефон</td>\r\n	<td>phone</td>\r\n</tr>\r\n<tr>\r\n	<td>Email</td>\r\n	<td>email</td>\r\n</tr>\r\n</tbody>\r\n</table>',1),(3,'napisat-nam',3,'Написать нам','','replay@td-uspekh.rf','tduspeh56@yandex.ru','Форма - написать нам','<table>\r\n<tbody>\r\n<tr>\r\n	<td>Телефон\r\n	</td>\r\n	<td>phone\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Имя\r\n	</td>\r\n	<td>name\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Сообщение\r\n	</td>\r\n	<td>body\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>',1);
/*!40000 ALTER TABLE `yupe_mail_mail_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_menu_menu`
--

DROP TABLE IF EXISTS `yupe_menu_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_menu_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_menu_menu_code` (`code`),
  KEY `ix_yupe_menu_menu_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_menu_menu`
--

LOCK TABLES `yupe_menu_menu` WRITE;
/*!40000 ALTER TABLE `yupe_menu_menu` DISABLE KEYS */;
INSERT INTO `yupe_menu_menu` VALUES (1,'Верхнее меню','top-menu','Основное меню сайта, расположенное сверху в блоке mainmenu.',1),(2,'Меню в подвале 1','footer-menu-1','Меню в подвале 1',1),(3,'Меню в подвале 2','footer-menu-2','Меню в подвале 2',1);
/*!40000 ALTER TABLE `yupe_menu_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_menu_menu_item`
--

DROP TABLE IF EXISTS `yupe_menu_menu_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_menu_menu_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `regular_link` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(150) NOT NULL,
  `href` varchar(150) NOT NULL,
  `class` varchar(150) DEFAULT NULL,
  `title_attr` varchar(150) DEFAULT NULL,
  `before_link` varchar(150) DEFAULT NULL,
  `after_link` varchar(150) DEFAULT NULL,
  `target` varchar(150) DEFAULT NULL,
  `rel` varchar(150) DEFAULT NULL,
  `condition_name` varchar(150) DEFAULT '0',
  `condition_denial` int(11) DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `entity_module_name` varchar(40) DEFAULT NULL,
  `entity_name` varchar(40) DEFAULT NULL,
  `entity_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_yupe_menu_menu_item_menu_id` (`menu_id`),
  KEY `ix_yupe_menu_menu_item_sort` (`sort`),
  KEY `ix_yupe_menu_menu_item_status` (`status`),
  CONSTRAINT `fk_yupe_menu_menu_item_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `yupe_menu_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_menu_menu_item`
--

LOCK TABLES `yupe_menu_menu_item` WRITE;
/*!40000 ALTER TABLE `yupe_menu_menu_item` DISABLE KEYS */;
INSERT INTO `yupe_menu_menu_item` VALUES (1,0,1,1,'Продукция','/tovary','','Главная страница сайта','','','','','',0,1,1,'','',NULL),(2,0,1,1,'О нас','/o-nas','','Блоги','','','','','',0,2,1,'','',NULL),(9,0,1,1,'Партнерам','/partneram','','FAQ','','','','','',0,7,1,'','',NULL),(10,0,1,1,'Документы','/sertifikaty','','Контакты','','','','','',0,7,1,'','',NULL),(11,0,1,1,'Производство','/proizvodstvo','','Магазин','','','','','',0,1,1,'','',NULL),(12,0,1,1,'Вакансии','/vakansii','','','','','','','',0,8,1,'','',NULL),(13,0,1,1,'Новости','/','','','','','','','',0,9,1,'','',NULL),(14,0,1,1,'Контакты','/kontakty','','','','','','','',0,10,1,'','',NULL),(15,0,2,1,'Продукция','/tovary','','','','','','','',0,11,1,'','',NULL),(16,0,2,1,'Производство','/proizvodstvo','','','','','','','',0,12,1,'','',NULL),(17,0,2,1,'О нас','/o-nas','','','','','','','',0,13,1,'','',NULL),(18,0,2,1,'Партнерам','/partneram','','','','','','','',0,14,1,'','',NULL),(19,0,3,1,'Документы','/sertifikaty','','','','','','','',0,15,1,'','',NULL),(20,0,3,1,'Вакансии','/vakansii','','','','','','','',0,16,1,'','',NULL),(21,0,3,1,'Новости','/','','','','','','','',0,17,1,'','',NULL),(22,0,3,1,'Контакты','/kontakty','','','','','','','',0,18,1,'','',NULL);
/*!40000 ALTER TABLE `yupe_menu_menu_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_migrations`
--

DROP TABLE IF EXISTS `yupe_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_migrations_module` (`module`)
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_migrations`
--

LOCK TABLES `yupe_migrations` WRITE;
/*!40000 ALTER TABLE `yupe_migrations` DISABLE KEYS */;
INSERT INTO `yupe_migrations` VALUES (1,'user','m000000_000000_user_base',1565708547),(2,'user','m131019_212911_user_tokens',1565708547),(3,'user','m131025_152911_clean_user_table',1565708547),(4,'user','m131026_002234_prepare_hash_user_password',1565708547),(5,'user','m131106_111552_user_restore_fields',1565708548),(6,'user','m131121_190850_modify_tokes_table',1565708548),(7,'user','m140812_100348_add_expire_to_token_table',1565708548),(8,'user','m150416_113652_rename_fields',1565708548),(9,'user','m151006_000000_user_add_phone',1565708548),(10,'yupe','m000000_000000_yupe_base',1565708550),(11,'yupe','m130527_154455_yupe_change_unique_index',1565708550),(12,'yupe','m150416_125517_rename_fields',1565708550),(13,'yupe','m160204_195213_change_settings_type',1565708550),(14,'category','m000000_000000_category_base',1565708552),(15,'category','m150415_150436_rename_fields',1565708552),(16,'image','m000000_000000_image_base',1565708554),(17,'image','m150226_121100_image_order',1565708554),(18,'image','m150416_080008_rename_fields',1565708554),(19,'mail','m000000_000000_mail_base',1565708555),(20,'comment','m000000_000000_comment_base',1565708557),(21,'comment','m130704_095200_comment_nestedsets',1565708558),(22,'comment','m150415_151804_rename_fields',1565708558),(23,'notify','m141031_091039_add_notify_table',1565708558),(24,'blog','m000000_000000_blog_base',1565708560),(25,'blog','m130503_091124_BlogPostImage',1565708560),(26,'blog','m130529_151602_add_post_category',1565708561),(27,'blog','m140226_052326_add_community_fields',1565708561),(28,'blog','m140714_110238_blog_post_quote_type',1565708561),(29,'blog','m150406_094809_blog_post_quote_type',1565708561),(30,'blog','m150414_180119_rename_date_fields',1565708561),(31,'blog','m160518_175903_alter_blog_foreign_keys',1565708561),(32,'blog','m180421_143937_update_blog_meta_column',1565708561),(33,'blog','m180421_143938_add_post_meta_title_column',1565708562),(34,'page','m000000_000000_page_base',1565708567),(35,'page','m130115_155600_columns_rename',1565708567),(36,'page','m140115_083618_add_layout',1565708568),(37,'page','m140620_072543_add_view',1565708568),(38,'page','m150312_151049_change_body_type',1565708568),(39,'page','m150416_101038_rename_fields',1565708568),(40,'page','m180224_105407_meta_title_column',1565708568),(41,'page','m180421_143324_update_page_meta_column',1565708568),(42,'sitemap','m141004_130000_sitemap_page',1565708570),(43,'sitemap','m141004_140000_sitemap_page_data',1565708570),(44,'rbac','m140115_131455_auth_item',1565708572),(45,'rbac','m140115_132045_auth_item_child',1565708572),(46,'rbac','m140115_132319_auth_item_assign',1565708572),(47,'rbac','m140702_230000_initial_role_data',1565708573),(48,'menu','m000000_000000_menu_base',1565708574),(49,'menu','m121220_001126_menu_test_data',1565708575),(50,'menu','m160914_134555_fix_menu_item_default_values',1565708575),(51,'menu','m181214_110527_menu_item_add_entity_fields',1565708575),(52,'news','m000000_000000_news_base',1565708577),(53,'news','m150416_081251_rename_fields',1565708577),(54,'news','m180224_105353_meta_title_column',1565708577),(55,'news','m180421_142416_update_news_meta_column',1565708577),(56,'gallery','m000000_000000_gallery_base',1565708581),(57,'gallery','m130427_120500_gallery_creation_user',1565708581),(58,'gallery','m150416_074146_rename_fields',1565708581),(59,'gallery','m160514_131314_add_preview_to_gallery',1565708581),(60,'gallery','m160515_123559_add_category_to_gallery',1565708582),(61,'gallery','m160515_151348_add_position_to_gallery_image',1565708582),(62,'gallery','m181224_072816_add_sort_to_gallery',1565708582),(63,'feedback','m000000_000000_feedback_base',1565708583),(64,'feedback','m150415_184108_rename_fields',1565708584),(65,'contentblock','m000000_000000_contentblock_base',1565708585),(66,'contentblock','m140715_130737_add_category_id',1565708585),(67,'contentblock','m150127_130425_add_status_column',1565708585),(68,'slider','m000000_000000_slider_base',1565710738),(69,'slider','m000000_000001_slider_add_column_image_xs',1565710738),(70,'store','m140812_160000_store_attribute_group_base',1565714757),(71,'store','m140812_170000_store_attribute_base',1565714757),(72,'store','m140812_180000_store_attribute_option_base',1565714758),(73,'store','m140813_200000_store_category_base',1565714758),(74,'store','m140813_210000_store_type_base',1565714758),(75,'store','m140813_220000_store_type_attribute_base',1565714758),(76,'store','m140813_230000_store_producer_base',1565714758),(77,'store','m140814_000000_store_product_base',1565714758),(78,'store','m140814_000010_store_product_category_base',1565714758),(79,'store','m140814_000013_store_product_attribute_eav_base',1565714758),(80,'store','m140814_000018_store_product_image_base',1565714758),(81,'store','m140814_000020_store_product_variant_base',1565714759),(82,'store','m141014_210000_store_product_category_column',1565714759),(83,'store','m141015_170000_store_product_image_column',1565714759),(84,'store','m141218_091834_default_null',1565714759),(85,'store','m150210_063409_add_store_menu_item',1565714759),(86,'store','m150210_105811_add_price_column',1565714759),(87,'store','m150210_131238_order_category',1565714759),(88,'store','m150211_105453_add_position_for_product_variant',1565714759),(89,'store','m150226_065935_add_product_position',1565714759),(90,'store','m150416_112008_rename_fields',1565714759),(91,'store','m150417_180000_store_product_link_base',1565714759),(92,'store','m150825_184407_change_store_url',1565714759),(93,'store','m150907_084604_new_attributes',1565714760),(94,'store','m151218_081635_add_external_id_fields',1565714760),(95,'store','m151218_082939_add_external_id_ix',1565714760),(96,'store','m151218_142113_add_product_index',1565714760),(97,'store','m151223_140722_drop_product_type_categories',1565714760),(98,'store','m160210_084850_add_h1_and_canonical',1565714760),(99,'store','m160210_131541_add_main_image_alt_title',1565714760),(100,'store','m160211_180200_add_additional_images_alt_title',1565714760),(101,'store','m160215_110749_add_image_groups_table',1565714760),(102,'store','m160227_114934_rename_producer_order_column',1565714760),(103,'store','m160309_091039_add_attributes_sort_and_search_fields',1565714760),(104,'store','m160413_184551_add_type_attr_fk',1565714760),(105,'store','m160602_091243_add_position_product_index',1565714760),(106,'store','m160602_091909_add_producer_sort_index',1565714760),(107,'store','m160713_105449_remove_irrelevant_product_status',1565714760),(108,'store','m160805_070905_add_attribute_description',1565714760),(109,'store','m161015_121915_change_product_external_id_type',1565714761),(110,'store','m161122_090922_add_sort_product_position',1565714761),(111,'store','m161122_093736_add_store_layouts',1565714761),(112,'store','m181218_121815_store_product_variant_quantity_column',1565714761),(113,'page','m190421_143324_add_page_image_column',1565756048),(114,'page','m200421_143324_add_page_desc_column',1565963668),(115,'page','m200422_143324_add_page_upakov_column',1565971642),(116,'page','m200423_143324_add_page_upakov_column',1566053796),(117,'page','m200424_143324_add_page_big_image_column',1566465093);
/*!40000 ALTER TABLE `yupe_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_news_news`
--

DROP TABLE IF EXISTS `yupe_news_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_news_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `lang` char(2) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `date` date NOT NULL,
  `title` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `short_text` text,
  `full_text` text NOT NULL,
  `image` varchar(300) DEFAULT NULL,
  `link` varchar(300) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `meta_keywords` varchar(250) NOT NULL,
  `meta_description` varchar(250) NOT NULL,
  `meta_title` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_news_news_alias_lang` (`slug`,`lang`),
  KEY `ix_yupe_news_news_status` (`status`),
  KEY `ix_yupe_news_news_user_id` (`user_id`),
  KEY `ix_yupe_news_news_category_id` (`category_id`),
  KEY `ix_yupe_news_news_date` (`date`),
  CONSTRAINT `fk_yupe_news_news_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_news_news_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_news_news`
--

LOCK TABLES `yupe_news_news` WRITE;
/*!40000 ALTER TABLE `yupe_news_news` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_news_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_notify_settings`
--

DROP TABLE IF EXISTS `yupe_notify_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_notify_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `my_post` tinyint(1) NOT NULL DEFAULT '1',
  `my_comment` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_notify_settings_user_id` (`user_id`),
  CONSTRAINT `fk_yupe_notify_settings_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_notify_settings`
--

LOCK TABLES `yupe_notify_settings` WRITE;
/*!40000 ALTER TABLE `yupe_notify_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_notify_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_page_page`
--

DROP TABLE IF EXISTS `yupe_page_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_page_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `lang` char(2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `change_user_id` int(11) DEFAULT NULL,
  `title_short` varchar(150) NOT NULL,
  `title` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `body` mediumtext NOT NULL,
  `meta_keywords` varchar(250) NOT NULL,
  `meta_description` varchar(250) NOT NULL,
  `status` int(11) NOT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `layout` varchar(250) DEFAULT NULL,
  `view` varchar(250) DEFAULT NULL,
  `meta_title` varchar(250) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `desc` text,
  `upakov` text,
  `age` text,
  `mass` text,
  `uslovie` text,
  `sertificat` text,
  `desc2` text,
  `imageBig` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_page_page_slug_lang` (`slug`,`lang`),
  KEY `ix_yupe_page_page_status` (`status`),
  KEY `ix_yupe_page_page_is_protected` (`is_protected`),
  KEY `ix_yupe_page_page_user_id` (`user_id`),
  KEY `ix_yupe_page_page_change_user_id` (`change_user_id`),
  KEY `ix_yupe_page_page_menu_order` (`order`),
  KEY `ix_yupe_page_page_category_id` (`category_id`),
  CONSTRAINT `fk_yupe_page_page_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_page_page_change_user_id` FOREIGN KEY (`change_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_page_page_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_page_page`
--

LOCK TABLES `yupe_page_page` WRITE;
/*!40000 ALTER TABLE `yupe_page_page` DISABLE KEYS */;
INSERT INTO `yupe_page_page` VALUES (1,NULL,'ru',NULL,'2019-08-13 20:26:40','2019-08-13 20:27:31',1,1,'','Главная','glavnaya','<p>test</p>','','',0,0,1,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,NULL,'ru',NULL,'2019-08-13 21:55:06','2019-11-25 15:43:40',1,1,'','О нас','o-nas','<p>Общество с ограниченной ответственностью «Торговый дом «Успех» начало свою деятельность в начале 2017 года. На первых этапах своего развития компания работала в сфере оптовой торговли продуктами питания. Однако, очень скоро пришло понимание необходимости развития производства. Так было принято решение о запуске проекта «Цех по переработке овощей», который в кратчайшие сроки был успешно реализован Производственная база общества с ограниченной ответственностью «Торговый дом «Успех» представляет собой комплекс современного оборудования ведущих мировых производителей в сфере переработки овощей и отвечает высоким требованиям, предъявляемым к поточности при работе с продовольственными товарами.</p>  <p>Фактическое место размещения производственного цеха: РФ, Оренбургская область, г. Оренбург, проезд Мясокомбината, 2. Внутри помещения устанавливается строгий санитарный режим: все работники имеют действующие санитарные книжки, весь инвентарь и оборудование в надлежащем порядке проходит обработку, помещения своевременно дезинфицируются  и т.д. Для обеспечения нормативных показателей качества продукции производственный цех отвечает требуемому температурному режиму – не более 20 градусов тепла в рабочей зоне, 2-4<sup>о</sup> в холодильной камере, в которой хранится сырьё и готовая продукция.</p>','','',1,0,2,'','','',NULL,'','','','','','','',NULL),(3,NULL,'ru',NULL,'2019-08-13 21:59:52','2019-08-13 22:02:47',1,1,'','Партнерам','partneram','<p>Страница находится в разработке</p>','','',1,0,3,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,NULL,'ru',NULL,'2019-08-13 22:00:20','2019-08-13 22:02:44',1,1,'','Документы','dokumenty','<p>Страница находится в разработке</p>','','',1,0,4,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,NULL,'ru',NULL,'2019-08-13 22:00:30','2019-11-25 15:45:02',1,1,'','Вакансии','vakansii','<p>ООО «Торговый дом «Успех» приглашает на работу по вакансиям: </p><ul><li>оператор механизированной линии</li></ul><p>Оформление согласно трудовому законодательству.</p>','','',1,0,5,'','','',NULL,'','','','','','','',NULL),(6,NULL,'ru',NULL,'2019-08-13 22:00:40','2019-11-14 22:33:25',1,1,'','Контакты','kontakty','<p>Адрес: 460048, г. Оренбург,<br>улица Шевченко 30\r\n</p>\r\n<p>Адрес производства: 460009, г. Оренбург,<br>проезд Мясокомбината 2\r\n</p>\r\n<p>Тел.: +7 919 869-21-28\r\n</p>\r\n<p>E-mail: tduspeh56@yandex.ru\r\n</p>\r\n<br><br><br><br><br>','','',1,0,6,'','contact','',NULL,'','','','','','','',NULL),(7,NULL,'ru',NULL,'2019-08-13 22:00:51','2019-08-20 21:40:24',1,1,'','Производство','proizvodstvo','<p>Производственная база общества с ограниченной ответственностью «Торговый дом «Успех» представляет собой комплекс современного оборудования ведущих мировых производителей в сфере переработки овощей и отвечает высоким требованиям, предъявляемым к поточности при работе с продовольственными товарами.\r\n</p>\r\n<p>Адрес производства: Российская Федерация, Оренбургская область, г. Оренбург, проезд Мясокомбината, 2.\r\n</p>\r\n<p>Внутри помещения устанавливается строгий санитарный режим: все работники имеют действующие санитарные книжки, весь инвентарь и оборудование в надлежащем порядке проходит обработку, помещения своевременно дезинфицируются.\r\n</p>\r\n<p>Для обеспечения должного уровня качества продукции производственный цех отвечает требуемому температурному режиму – не более 20 градусов тепла в рабочей зоне, 2-4о в холодильной камере, в которой хранится сырьё и готовая продукция. Специальная упаковочная пленка для вакуумной среды обеспечивает необходимый барьер между внешней средой и продуктом при хранении.\r\n</p>\r\n<p>Производственная мощность линии по производству овощных полуфабрикатов составляет около 100 тонн готовой продукции в месяц, существует возможность увеличения производственной мощности.\r\n</p>\r\n<p>Общество с ограниченной ответственностью «Торговый дом «Успех» обеспечивает надежную логистику на территории Оренбургской области и за ее пределами.\r\n</p>\r\n<p><br><br>\r\n</p>\r\n<p><br>\r\n</p>','','',1,0,7,'','','',NULL,'','','','','','','',NULL),(8,NULL,'ru',NULL,'2019-08-14 08:53:48','2019-08-17 19:26:33',1,1,'','Товары','tovary','<p>Товары</p>','','',1,0,8,'','tovary','',NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL),(9,NULL,'ru',8,'2019-08-14 08:57:38','2019-09-02 18:04:09',1,1,'','ПФ картофель свежий очищенный, охлажденный','pf-kartofel-svezhiy-ochishchennyy-ohlazhdennyy','<p>ПФ картофель свежий очищенный, охлажденный</p>','','',1,0,9,'','item','','d6648e090cff358058289355eb1e1731.png','Полуфабрикат','(упаковано под вакуумом)','Не более 6 суток','от 0.5 кг. до 5 кг.','от +2 до +6 оС','<div class=\"col-md-6 col-sm-6 col-xs-12\">\r\n<div class=\"sertificat-item\">\r\n<span class=\"name\">Наименование документа:</span>\r\n<span class=\"desc\">Декларация соответствия</span>\r\n</div>\r\n</div>\r\n<div class=\"col-md-6 col-sm-6 col-xs-12\">\r\n<div class=\"sertificat-item\">\r\n<span class=\"name\">Срок действия:</span>\r\n<span class=\"desc\">от 05.07.2019 до 05.07.2023</span>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-6 col-sm-6 col-xs-12\">\r\n<div class=\"sertificat-item\">\r\n<span class=\"name\">Наименование продукции:</span>\r\n<span class=\"desc\">Полуфабрикаты продуктов питания, тестовое описание  продукции.</span>\r\n</div>\r\n</div>\r\n<div class=\"col-md-6 col-sm-6 col-xs-12\">\r\n<div class=\"sertificat-item\">\r\n<span class=\"name\">Номер документа:</span>\r\n<span class=\"desc\">АВВ799 19 ТВоГаи-7785</span>\r\n<a href=\"#\">Скачать сертификат</a>\r\n</div>\r\n</div>','упаковано под вакуумом','a184d9cfbed5a8a777b34c37e01a2e5a.png'),(10,NULL,'ru',8,'2019-08-14 08:57:59','2019-09-02 18:04:04',1,1,'','ПФ морковь свежая очищенная, охлажденная','pf-morkov-svezhaya-ochishchennaya-ohlazhdennaya','<p>ПФ морковь свежая очищенная, охлажденная</p>','','',1,0,10,'','item','','dfb1b57c859068a8e6077c1daaa60bc9.png','Полуфабрикат','(упаковано под вакуумом)','Не более 5 суток','от 0.5 кг. до 5 кг.','от +2 до +6 оС','<div class=\"col-md-6 col-sm-6 col-xs-12\">\r\n<div class=\"sertificat-item\">\r\n<span class=\"name\">Наименование документа:</span>\r\n<span class=\"desc\">Декларация соответствия</span>\r\n</div>\r\n</div>\r\n<div class=\"col-md-6 col-sm-6 col-xs-12\">\r\n<div class=\"sertificat-item\">\r\n<span class=\"name\">Срок действия:</span>\r\n<span class=\"desc\">от 05.07.2019 до 05.07.2023</span>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-6 col-sm-6 col-xs-12\">\r\n<div class=\"sertificat-item\">\r\n<span class=\"name\">Наименование продукции:</span>\r\n<span class=\"desc\">Полуфабрикаты продуктов питания, тестовое описание  продукции.</span>\r\n</div>\r\n</div>\r\n<div class=\"col-md-6 col-sm-6 col-xs-12\">\r\n<div class=\"sertificat-item\">\r\n<span class=\"name\">Номер документа:</span>\r\n<span class=\"desc\">АВВ799 19 ТВоГаи-7785</span>\r\n<a href=\"#\">Скачать сертификат</a>\r\n</div>\r\n</div>','упаковано под вакуумом','28694dbd508238b4e360eeec0f51da69.png'),(11,NULL,'ru',8,'2019-08-14 08:58:19','2019-09-02 18:03:59',1,1,'','ПФ свекла свежая очищенная, охлажденная','pf-svekla-svezhaya-ochishchennaya-ohlazhdennaya','<p>ПФ свекла свежая очищенная, охлажденная </p>','','',1,0,11,'','item','','ca0ec5b4e35b215a66e14768337f73df.png','Полуфабрикат','(упаковано под вакуумом)','Не более 5 суток','от 0.5 кг. до 5 кг.','от +2 до +6 оС','<div class=\"col-md-6 col-sm-6 col-xs-12\">\r\n<div class=\"sertificat-item\">\r\n<span class=\"name\">Наименование документа:</span>\r\n<span class=\"desc\">Декларация соответствия</span>\r\n</div>\r\n</div>\r\n<div class=\"col-md-6 col-sm-6 col-xs-12\">\r\n<div class=\"sertificat-item\">\r\n<span class=\"name\">Срок действия:</span>\r\n<span class=\"desc\">от 05.07.2019 до 05.07.2023</span>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-6 col-sm-6 col-xs-12\">\r\n<div class=\"sertificat-item\">\r\n<span class=\"name\">Наименование продукции:</span>\r\n<span class=\"desc\">Полуфабрикаты продуктов питания, тестовое описание  продукции.</span>\r\n</div>\r\n</div>\r\n<div class=\"col-md-6 col-sm-6 col-xs-12\">\r\n<div class=\"sertificat-item\">\r\n<span class=\"name\">Номер документа:</span>\r\n<span class=\"desc\">АВВ799 19 ТВоГаи-7785</span>\r\n<a href=\"#\">Скачать сертификат</a>\r\n</div>\r\n</div>','упаковано под вакуумом','d1c31a8c8ae6a9a53f35c556d7160dcc.png'),(12,NULL,'ru',8,'2019-08-14 08:58:33','2019-09-02 18:03:53',1,1,'','ПФ лук репчатый очищенный, охлажденный','pf-luk-repchatyy-ochishchennyy-ohlazhdennyy','<p>ПФ лук репчатый очищенный, охлажденный </p>','','',1,0,12,'','item','','162a8ead9b15a1766b3a39716c0fa314.png','Полуфабрикат','(упаковано под вакуумом)','Не более 5 суток','от 0.5 кг. до 5 кг.','от +2 до +6 оС','<div class=\"col-md-6 col-sm-6 col-xs-12\">\r\n<div class=\"sertificat-item\">\r\n<span class=\"name\">Наименование документа:</span>\r\n<span class=\"desc\">Декларация соответствия</span>\r\n</div>\r\n</div>\r\n<div class=\"col-md-6 col-sm-6 col-xs-12\">\r\n<div class=\"sertificat-item\">\r\n<span class=\"name\">Срок действия:</span>\r\n<span class=\"desc\">от 05.07.2019 до 05.07.2023</span>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-6 col-sm-6 col-xs-12\">\r\n<div class=\"sertificat-item\">\r\n<span class=\"name\">Наименование продукции:</span>\r\n<span class=\"desc\">Полуфабрикаты продуктов питания, тестовое описание  продукции.</span>\r\n</div>\r\n</div>\r\n<div class=\"col-md-6 col-sm-6 col-xs-12\">\r\n<div class=\"sertificat-item\">\r\n<span class=\"name\">Номер документа:</span>\r\n<span class=\"desc\">АВВ799 19 ТВоГаи-7785</span>\r\n<a href=\"#\">Скачать сертификат</a>\r\n</div>\r\n</div>','упаковано под вакуумом','9f2e5d278051e41dc48f2961b0e50bb6.png'),(13,NULL,'ru',8,'2019-08-14 08:58:52','2019-09-02 18:02:40',1,1,'','ПФ капуста белокочанная, охлажденная','pf-kapusta-belokochannaya-ohlazhdennaya','<p>ПФ капуста белокочанная, охлажденная  </p>','','',1,0,13,'','item','','b22371492cd133d4ec031104efd29501.png','Полуфабрикат','(упаковано под вакуумом)','Не более 5 суток','от 0.5 кг. до 5 кг.','от +2 до +6 оС','<div class=\"col-md-6 col-sm-6 col-xs-12\">\r\n<div class=\"sertificat-item\">\r\n<span class=\"name\">Наименование документа:</span>\r\n<span class=\"desc\">Декларация соответствия</span>\r\n</div>\r\n</div>\r\n<div class=\"col-md-6 col-sm-6 col-xs-12\">\r\n<div class=\"sertificat-item\">\r\n<span class=\"name\">Срок действия:</span>\r\n<span class=\"desc\">от 05.07.2019 до 05.07.2023</span>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-6 col-sm-6 col-xs-12\">\r\n<div class=\"sertificat-item\">\r\n<span class=\"name\">Наименование продукции:</span>\r\n<span class=\"desc\">Полуфабрикаты продуктов питания, тестовое описание  продукции.</span>\r\n</div>\r\n</div>\r\n<div class=\"col-md-6 col-sm-6 col-xs-12\">\r\n<div class=\"sertificat-item\">\r\n<span class=\"name\">Номер документа:</span>\r\n<span class=\"desc\">АВВ799 19 ТВоГаи-7785</span>\r\n<a href=\"#\">Скачать сертификат</a>\r\n</div>\r\n</div>','упаковано под вакуумом','5660f0608f47393ef862a1e185769f68.png'),(14,NULL,'ru',NULL,'2019-08-16 21:42:13','2019-08-16 21:42:13',1,1,'','Акции','akcii','<p>Акции</p>','','',1,0,14,'','','',NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL),(15,NULL,'ru',14,'2019-08-16 21:44:11','2019-08-20 18:55:16',1,1,'','Овощи под реализацию','ovoshchi-pod-realizaciyu','<p>Баннер с описанием продукта, который было бы актуально заказать. Текст тестовый и будет заменен вашим.</p>','','',1,0,15,'','','','cd9935bd92ba8ccc925530020c44bd8c.png','Мы предлагаем','','','','','','',NULL),(16,NULL,'ru',14,'2019-08-16 21:45:43','2019-08-20 19:10:32',1,1,'','Запасаемся к лету','zapasaemsya-k-letu','<p>Баннер с описанием продукта, который было бы актуально заказать. Текст тестовый и будет заменен вашим.\r\n</p>','','',1,0,16,'','','','044518239e29fda79356f414f88cb9ef.png','Ограниченная партия','','','','','','',NULL),(17,NULL,'ru',NULL,'2019-08-17 20:19:46','2019-11-25 15:49:24',1,1,'','Сертификаты','sertifikaty','<p>Сертификаты</p>','','',1,0,17,'','sertificats','',NULL,'','','','','','','',NULL),(18,NULL,'ru',17,'2019-08-17 20:20:39','2019-09-17 22:24:42',1,1,'','Сертификат 1','sertifikat-1','<p>Сертификат соответствия\r\n                    требованиям ГОСТ Р 57976-2017\r\n                    - хранение свежих овощей\r\n                и фруктов.</p>','','',1,0,18,'','','','28b34e1fdabe983a68d5f7bb654dcbae.jpg','','','','','','','',NULL),(19,NULL,'ru',17,'2019-08-17 20:21:00','2019-09-17 22:24:51',1,1,'','Сертификат 2','sertifikat-2','<p>Сертификат соответствия требованиям ГОСТ Р 57976-2017 - хранение свежих овощей и фруктов.</p>','','',1,0,19,'','','','336d349a660702a9cff89edf91fdf7a2.jpg','','','','','','','',NULL),(20,NULL,'ru',17,'2019-08-17 20:21:17','2019-08-28 16:56:41',1,1,'','Сертификат 3','sertifikat-3','<p>Сертификат соответствия требованиям ГОСТ Р 57976-2017 - хранение свежих овощей и фруктов.</p>','','',0,0,20,'','','','dcd34d67c9067289110bcc5d7e3b9db8.png','','','','','','','',NULL),(21,NULL,'ru',17,'2019-08-17 20:21:43','2019-08-28 16:56:47',1,1,'','Сертификат 4','sertifikat-4','<p>Сертификат соответствия требованиям ГОСТ Р 57976-2017 - хранение свежих овощей и фруктов.</p>','','',0,0,21,'','','','6109069f1013cf7a4f5759d19b868660.png','','','','','','','',NULL);
/*!40000 ALTER TABLE `yupe_page_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_sitemap_page`
--

DROP TABLE IF EXISTS `yupe_sitemap_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_sitemap_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(250) NOT NULL,
  `changefreq` varchar(20) NOT NULL,
  `priority` float NOT NULL DEFAULT '0.5',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_sitemap_page_url` (`url`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_sitemap_page`
--

LOCK TABLES `yupe_sitemap_page` WRITE;
/*!40000 ALTER TABLE `yupe_sitemap_page` DISABLE KEYS */;
INSERT INTO `yupe_sitemap_page` VALUES (1,'/','daily',0.5,1);
/*!40000 ALTER TABLE `yupe_sitemap_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_slider`
--

DROP TABLE IF EXISTS `yupe_slider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT 'Название',
  `name_short` varchar(255) DEFAULT NULL COMMENT 'Короткое Название',
  `image` varchar(255) DEFAULT NULL COMMENT 'Изображение',
  `description` text COMMENT 'Описание',
  `description_short` text COMMENT 'Краткое описание',
  `button_name` varchar(255) DEFAULT NULL COMMENT 'Название кнопки',
  `button_link` varchar(255) DEFAULT NULL COMMENT 'url для кнопки',
  `status` int(11) DEFAULT NULL COMMENT 'Статус',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка',
  `image_xs` varchar(255) DEFAULT NULL COMMENT 'Изображение',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_slider`
--

LOCK TABLES `yupe_slider` WRITE;
/*!40000 ALTER TABLE `yupe_slider` DISABLE KEYS */;
INSERT INTO `yupe_slider` VALUES (1,'Оптовые поставки','<span>вакуумированных</span><br>овощей','c0ff53a820940107646849ca50a15d3f.jpg','Мы знаем об упаковке овощей все и готовы этим делиться..','для вашего бизнеса','','',1,1,NULL);
/*!40000 ALTER TABLE `yupe_slider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_attribute`
--

DROP TABLE IF EXISTS `yupe_store_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `unit` varchar(30) DEFAULT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0',
  `is_filter` smallint(6) NOT NULL DEFAULT '1',
  `description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_store_attribute_name_group` (`name`,`group_id`),
  KEY `ix_yupe_store_attribute_title` (`title`),
  KEY `fk_yupe_store_attribute_group` (`group_id`),
  CONSTRAINT `fk_yupe_store_attribute_group` FOREIGN KEY (`group_id`) REFERENCES `yupe_store_attribute_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_attribute`
--

LOCK TABLES `yupe_store_attribute` WRITE;
/*!40000 ALTER TABLE `yupe_store_attribute` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_store_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_attribute_group`
--

DROP TABLE IF EXISTS `yupe_store_attribute_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_attribute_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_attribute_group`
--

LOCK TABLES `yupe_store_attribute_group` WRITE;
/*!40000 ALTER TABLE `yupe_store_attribute_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_store_attribute_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_attribute_option`
--

DROP TABLE IF EXISTS `yupe_store_attribute_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_attribute_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) DEFAULT NULL,
  `position` tinyint(4) DEFAULT NULL,
  `value` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_store_attribute_option_attribute_id` (`attribute_id`),
  KEY `ix_yupe_store_attribute_option_position` (`position`),
  CONSTRAINT `fk_yupe_store_attribute_option_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `yupe_store_attribute` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_attribute_option`
--

LOCK TABLES `yupe_store_attribute_option` WRITE;
/*!40000 ALTER TABLE `yupe_store_attribute_option` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_store_attribute_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_category`
--

DROP TABLE IF EXISTS `yupe_store_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(150) NOT NULL,
  `name` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text,
  `meta_title` varchar(250) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `meta_keywords` varchar(250) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '1',
  `external_id` varchar(100) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `meta_canonical` varchar(255) DEFAULT NULL,
  `image_alt` varchar(255) DEFAULT NULL,
  `image_title` varchar(255) DEFAULT NULL,
  `view` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_store_category_alias` (`slug`),
  KEY `ix_yupe_store_category_parent_id` (`parent_id`),
  KEY `ix_yupe_store_category_status` (`status`),
  KEY `yupe_store_category_external_id_ix` (`external_id`),
  CONSTRAINT `fk_yupe_store_category_parent` FOREIGN KEY (`parent_id`) REFERENCES `yupe_store_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_category`
--

LOCK TABLES `yupe_store_category` WRITE;
/*!40000 ALTER TABLE `yupe_store_category` DISABLE KEYS */;
INSERT INTO `yupe_store_category` VALUES (1,NULL,'produkty','Продукты',NULL,'','','','','',1,1,NULL,'','','','','');
/*!40000 ALTER TABLE `yupe_store_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_producer`
--

DROP TABLE IF EXISTS `yupe_store_producer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_producer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_short` varchar(150) NOT NULL,
  `name` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text,
  `meta_title` varchar(250) DEFAULT NULL,
  `meta_keywords` varchar(250) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '0',
  `view` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_yupe_store_producer_slug` (`slug`),
  KEY `ix_yupe_store_producer_sort` (`sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_producer`
--

LOCK TABLES `yupe_store_producer` WRITE;
/*!40000 ALTER TABLE `yupe_store_producer` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_store_producer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_product`
--

DROP TABLE IF EXISTS `yupe_store_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) DEFAULT NULL,
  `producer_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sku` varchar(100) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `price` decimal(19,3) NOT NULL DEFAULT '0.000',
  `discount_price` decimal(19,3) DEFAULT NULL,
  `discount` decimal(19,3) DEFAULT NULL,
  `description` text,
  `short_description` text,
  `data` text,
  `is_special` tinyint(1) NOT NULL DEFAULT '0',
  `length` decimal(19,3) DEFAULT NULL,
  `width` decimal(19,3) DEFAULT NULL,
  `height` decimal(19,3) DEFAULT NULL,
  `weight` decimal(19,3) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `in_stock` tinyint(4) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `meta_title` varchar(250) DEFAULT NULL,
  `meta_keywords` varchar(250) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `average_price` decimal(19,3) DEFAULT NULL,
  `purchase_price` decimal(19,3) DEFAULT NULL,
  `recommended_price` decimal(19,3) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `external_id` varchar(100) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `meta_canonical` varchar(255) DEFAULT NULL,
  `image_alt` varchar(255) DEFAULT NULL,
  `image_title` varchar(255) DEFAULT NULL,
  `view` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_store_product_alias` (`slug`),
  KEY `ix_yupe_store_product_status` (`status`),
  KEY `ix_yupe_store_product_type_id` (`type_id`),
  KEY `ix_yupe_store_product_producer_id` (`producer_id`),
  KEY `ix_yupe_store_product_price` (`price`),
  KEY `ix_yupe_store_product_discount_price` (`discount_price`),
  KEY `ix_yupe_store_product_create_time` (`create_time`),
  KEY `ix_yupe_store_product_update_time` (`update_time`),
  KEY `fk_yupe_store_product_category` (`category_id`),
  KEY `yupe_store_product_external_id_ix` (`external_id`),
  KEY `ix_yupe_store_product_sku` (`sku`),
  KEY `ix_yupe_store_product_name` (`name`),
  KEY `ix_yupe_store_product_position` (`position`),
  CONSTRAINT `fk_yupe_store_product_category` FOREIGN KEY (`category_id`) REFERENCES `yupe_store_category` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_product_producer` FOREIGN KEY (`producer_id`) REFERENCES `yupe_store_producer` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_product_type` FOREIGN KEY (`type_id`) REFERENCES `yupe_store_type` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_product`
--

LOCK TABLES `yupe_store_product` WRITE;
/*!40000 ALTER TABLE `yupe_store_product` DISABLE KEYS */;
INSERT INTO `yupe_store_product` VALUES (1,NULL,NULL,1,'','Картофель очищенный охлажденный','kartofel-ochishchennyy-ohlazhdennyy',0.000,NULL,NULL,'<p>Срок годности при\r\n                                температуре от +2 до +6 ,\r\n                                не более 6 суток</p>','','',0,NULL,NULL,NULL,NULL,NULL,1,1,'2019-08-13 21:51:31','2019-08-13 21:51:31','','','','4f660b493e28cb113e83a73fe1b3ddfb.png',NULL,NULL,NULL,1,NULL,'','','','','');
/*!40000 ALTER TABLE `yupe_store_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_product_attribute_value`
--

DROP TABLE IF EXISTS `yupe_store_product_attribute_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_product_attribute_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `number_value` double DEFAULT NULL,
  `string_value` varchar(250) DEFAULT NULL,
  `text_value` text,
  `option_value` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `yupe_fk_product_attribute_product` (`product_id`),
  KEY `yupe_fk_product_attribute_attribute` (`attribute_id`),
  KEY `yupe_fk_product_attribute_option` (`option_value`),
  KEY `yupe_ix_product_attribute_number_value` (`number_value`),
  KEY `yupe_ix_product_attribute_string_value` (`string_value`),
  CONSTRAINT `yupe_fk_product_attribute_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `yupe_store_attribute` (`id`) ON DELETE CASCADE,
  CONSTRAINT `yupe_fk_product_attribute_option` FOREIGN KEY (`option_value`) REFERENCES `yupe_store_attribute_option` (`id`) ON DELETE CASCADE,
  CONSTRAINT `yupe_fk_product_attribute_product` FOREIGN KEY (`product_id`) REFERENCES `yupe_store_product` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_product_attribute_value`
--

LOCK TABLES `yupe_store_product_attribute_value` WRITE;
/*!40000 ALTER TABLE `yupe_store_product_attribute_value` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_store_product_attribute_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_product_category`
--

DROP TABLE IF EXISTS `yupe_store_product_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_product_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_yupe_store_product_category_product_id` (`product_id`),
  KEY `ix_yupe_store_product_category_category_id` (`category_id`),
  CONSTRAINT `fk_yupe_store_product_category_category` FOREIGN KEY (`category_id`) REFERENCES `yupe_store_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_product_category_product` FOREIGN KEY (`product_id`) REFERENCES `yupe_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_product_category`
--

LOCK TABLES `yupe_store_product_category` WRITE;
/*!40000 ALTER TABLE `yupe_store_product_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_store_product_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_product_image`
--

DROP TABLE IF EXISTS `yupe_store_product_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_product_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `alt` varchar(255) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_yupe_store_product_image_product` (`product_id`),
  KEY `fk_yupe_store_product_image_group` (`group_id`),
  CONSTRAINT `fk_yupe_store_product_image_group` FOREIGN KEY (`group_id`) REFERENCES `yupe_store_product_image_group` (`id`) ON DELETE NO ACTION ON UPDATE SET NULL,
  CONSTRAINT `fk_yupe_store_product_image_product` FOREIGN KEY (`product_id`) REFERENCES `yupe_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_product_image`
--

LOCK TABLES `yupe_store_product_image` WRITE;
/*!40000 ALTER TABLE `yupe_store_product_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_store_product_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_product_image_group`
--

DROP TABLE IF EXISTS `yupe_store_product_image_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_product_image_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_product_image_group`
--

LOCK TABLES `yupe_store_product_image_group` WRITE;
/*!40000 ALTER TABLE `yupe_store_product_image_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_store_product_image_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_product_link`
--

DROP TABLE IF EXISTS `yupe_store_product_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_product_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `linked_product_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_store_product_link_product` (`product_id`,`linked_product_id`),
  KEY `fk_yupe_store_product_link_linked_product` (`linked_product_id`),
  KEY `fk_yupe_store_product_link_type` (`type_id`),
  CONSTRAINT `fk_yupe_store_product_link_linked_product` FOREIGN KEY (`linked_product_id`) REFERENCES `yupe_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_product_link_product` FOREIGN KEY (`product_id`) REFERENCES `yupe_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_product_link_type` FOREIGN KEY (`type_id`) REFERENCES `yupe_store_product_link_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_product_link`
--

LOCK TABLES `yupe_store_product_link` WRITE;
/*!40000 ALTER TABLE `yupe_store_product_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_store_product_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_product_link_type`
--

DROP TABLE IF EXISTS `yupe_store_product_link_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_product_link_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_store_product_link_type_code` (`code`),
  UNIQUE KEY `ux_yupe_store_product_link_type_title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_product_link_type`
--

LOCK TABLES `yupe_store_product_link_type` WRITE;
/*!40000 ALTER TABLE `yupe_store_product_link_type` DISABLE KEYS */;
INSERT INTO `yupe_store_product_link_type` VALUES (1,'similar','Похожие'),(2,'related','Сопутствующие');
/*!40000 ALTER TABLE `yupe_store_product_link_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_product_variant`
--

DROP TABLE IF EXISTS `yupe_store_product_variant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_product_variant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `attribute_value` varchar(255) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `type` tinyint(4) NOT NULL,
  `sku` varchar(50) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_yupe_store_product_variant_product` (`product_id`),
  KEY `idx_yupe_store_product_variant_attribute` (`attribute_id`),
  KEY `idx_yupe_store_product_variant_value` (`attribute_value`),
  CONSTRAINT `fk_yupe_store_product_variant_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `yupe_store_attribute` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_product_variant_product` FOREIGN KEY (`product_id`) REFERENCES `yupe_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_product_variant`
--

LOCK TABLES `yupe_store_product_variant` WRITE;
/*!40000 ALTER TABLE `yupe_store_product_variant` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_store_product_variant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_type`
--

DROP TABLE IF EXISTS `yupe_store_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_store_type_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_type`
--

LOCK TABLES `yupe_store_type` WRITE;
/*!40000 ALTER TABLE `yupe_store_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_store_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_store_type_attribute`
--

DROP TABLE IF EXISTS `yupe_store_type_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_store_type_attribute` (
  `type_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  PRIMARY KEY (`type_id`,`attribute_id`),
  KEY `fk_yupe_store_type_attribute_attribute` (`attribute_id`),
  CONSTRAINT `fk_yupe_store_type_attribute_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `yupe_store_attribute` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_type_attribute_type` FOREIGN KEY (`type_id`) REFERENCES `yupe_store_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_store_type_attribute`
--

LOCK TABLES `yupe_store_type_attribute` WRITE;
/*!40000 ALTER TABLE `yupe_store_type_attribute` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_store_type_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_user_tokens`
--

DROP TABLE IF EXISTS `yupe_user_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_user_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `expire_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_yupe_user_tokens_user_id` (`user_id`),
  CONSTRAINT `fk_yupe_user_tokens_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_user_tokens`
--

LOCK TABLES `yupe_user_tokens` WRITE;
/*!40000 ALTER TABLE `yupe_user_tokens` DISABLE KEYS */;
INSERT INTO `yupe_user_tokens` VALUES (37,1,'ZCeCKvXI2PYWugHaHh5R_nezJi0dDL9k',4,0,'2019-11-25 15:40:07','2019-11-25 15:40:07','79.140.25.135','2019-12-02 15:40:07');
/*!40000 ALTER TABLE `yupe_user_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_user_user`
--

DROP TABLE IF EXISTS `yupe_user_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_user_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `update_time` datetime NOT NULL,
  `first_name` varchar(250) DEFAULT NULL,
  `middle_name` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  `nick_name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '0',
  `birth_date` date DEFAULT NULL,
  `site` varchar(250) NOT NULL DEFAULT '',
  `about` varchar(250) NOT NULL DEFAULT '',
  `location` varchar(250) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '2',
  `access_level` int(11) NOT NULL DEFAULT '0',
  `visit_time` datetime DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `avatar` varchar(150) DEFAULT NULL,
  `hash` varchar(255) NOT NULL DEFAULT 'de46cb2d30cac4b03aa1100980d0d4700.72238200 1565708547',
  `email_confirm` tinyint(1) NOT NULL DEFAULT '0',
  `phone` char(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_user_user_nick_name` (`nick_name`),
  UNIQUE KEY `ux_yupe_user_user_email` (`email`),
  KEY `ix_yupe_user_user_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_user_user`
--

LOCK TABLES `yupe_user_user` WRITE;
/*!40000 ALTER TABLE `yupe_user_user` DISABLE KEYS */;
INSERT INTO `yupe_user_user` VALUES (1,'2019-08-13 20:03:41','','','','admin','monshtrina@yandex.ru',0,NULL,'','','',1,1,'2019-11-25 15:40:07','2019-08-13 20:03:41',NULL,'$2y$13$2cbaAgq8ZU8ahc1MdGF3nOUhGa1bQWfs6CAmbPqvFw2teAUlNXw6G',1,NULL);
/*!40000 ALTER TABLE `yupe_user_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_user_user_auth_assignment`
--

DROP TABLE IF EXISTS `yupe_user_user_auth_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_user_user_auth_assignment` (
  `itemname` char(64) NOT NULL,
  `userid` int(11) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`),
  KEY `fk_yupe_user_user_auth_assignment_user` (`userid`),
  CONSTRAINT `fk_yupe_user_user_auth_assignment_item` FOREIGN KEY (`itemname`) REFERENCES `yupe_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_user_user_auth_assignment_user` FOREIGN KEY (`userid`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_user_user_auth_assignment`
--

LOCK TABLES `yupe_user_user_auth_assignment` WRITE;
/*!40000 ALTER TABLE `yupe_user_user_auth_assignment` DISABLE KEYS */;
INSERT INTO `yupe_user_user_auth_assignment` VALUES ('admin',1,NULL,NULL);
/*!40000 ALTER TABLE `yupe_user_user_auth_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_user_user_auth_item`
--

DROP TABLE IF EXISTS `yupe_user_user_auth_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_user_user_auth_item` (
  `name` char(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`),
  KEY `ix_yupe_user_user_auth_item_type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_user_user_auth_item`
--

LOCK TABLES `yupe_user_user_auth_item` WRITE;
/*!40000 ALTER TABLE `yupe_user_user_auth_item` DISABLE KEYS */;
INSERT INTO `yupe_user_user_auth_item` VALUES ('admin',2,'Администратор',NULL,NULL);
/*!40000 ALTER TABLE `yupe_user_user_auth_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_user_user_auth_item_child`
--

DROP TABLE IF EXISTS `yupe_user_user_auth_item_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_user_user_auth_item_child` (
  `parent` char(64) NOT NULL,
  `child` char(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `fk_yupe_user_user_auth_item_child_child` (`child`),
  CONSTRAINT `fk_yupe_user_user_auth_item_child_child` FOREIGN KEY (`child`) REFERENCES `yupe_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_user_user_auth_itemchild_parent` FOREIGN KEY (`parent`) REFERENCES `yupe_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_user_user_auth_item_child`
--

LOCK TABLES `yupe_user_user_auth_item_child` WRITE;
/*!40000 ALTER TABLE `yupe_user_user_auth_item_child` DISABLE KEYS */;
/*!40000 ALTER TABLE `yupe_user_user_auth_item_child` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yupe_yupe_settings`
--

DROP TABLE IF EXISTS `yupe_yupe_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yupe_yupe_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` varchar(100) NOT NULL,
  `param_name` varchar(100) NOT NULL,
  `param_value` varchar(500) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_yupe_settings_module_id_param_name_user_id` (`module_id`,`param_name`,`user_id`),
  KEY `ix_yupe_yupe_settings_module_id` (`module_id`),
  KEY `ix_yupe_yupe_settings_param_name` (`param_name`),
  KEY `fk_yupe_yupe_settings_user_id` (`user_id`),
  CONSTRAINT `fk_yupe_yupe_settings_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yupe_yupe_settings`
--

LOCK TABLES `yupe_yupe_settings` WRITE;
/*!40000 ALTER TABLE `yupe_yupe_settings` DISABLE KEYS */;
INSERT INTO `yupe_yupe_settings` VALUES (1,'yupe','siteDescription','Успех','2019-08-13 20:04:01','2019-08-13 20:04:01',1,1),(2,'yupe','siteName',' Успех','2019-08-13 20:04:01','2019-08-13 20:04:01',1,1),(3,'yupe','siteKeyWords','Успех','2019-08-13 20:04:01','2019-08-13 20:04:01',1,1),(4,'yupe','email','monshtrina@yandex.ru','2019-08-13 20:04:01','2019-08-13 20:04:01',1,1),(5,'yupe','theme','default','2019-08-13 20:04:01','2019-08-13 20:04:01',1,1),(6,'yupe','backendTheme','','2019-08-13 20:04:01','2019-08-13 20:04:01',1,1),(7,'yupe','defaultLanguage','ru','2019-08-13 20:04:01','2019-08-13 20:04:01',1,1),(8,'yupe','defaultBackendLanguage','ru','2019-08-13 20:04:01','2019-08-13 20:04:01',1,1),(9,'homepage','mode','2','2019-08-13 20:27:02','2019-08-13 20:27:02',1,1),(10,'homepage','target','1','2019-08-13 20:27:02','2019-08-13 20:27:05',1,1),(11,'homepage','limit','','2019-08-13 20:27:02','2019-08-13 20:27:02',1,1),(12,'menuitem','pageSize','20','2019-08-17 19:25:39','2019-08-17 19:25:39',1,2),(13,'contentblock','pageSize','50','2019-08-20 13:49:07','2019-08-20 13:49:07',1,2),(14,'page','pageSize','50','2019-11-14 22:33:05','2019-11-14 22:33:05',1,2);
/*!40000 ALTER TABLE `yupe_yupe_settings` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-26 11:59:27
