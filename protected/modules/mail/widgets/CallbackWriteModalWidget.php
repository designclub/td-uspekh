<?php
/**
 * CallbackModalWidget виджет формы "Заказать звонок"
 */
Yii::import('application.modules.mail.models.form.CallbackWriteFormModal');

class CallbackWriteModalWidget extends yupe\widgets\YWidget
{
    public $view = 'callback-write-widget';

    public function run()
    {
        $model = new CallbackWriteFormModal;
        if (isset($_POST['CallbackWriteFormModal'])) {
            $model->attributes = $_POST['CallbackWriteFormModal'];
            if($model->verify == ''){
                if ($model->validate()) {
                    Yii::app()->user->setFlash('callback-success2', Yii::t('MailModule.mail', 'Your request has been successfully sent.'));
                    Yii::app()->controller->refresh();
                }
            }
        }

        $this->render($this->view, [
            'model' => $model,
        ]);
    }

}
