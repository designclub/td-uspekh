<?php

Yii::import('application.modules.page.models.*');

class PagesNewWidget extends yupe\widgets\YWidget
{
    public $id;
    public $parent_id;
    public $butname;
    public $limit;
    
    public $view = 'pageswidget';

    protected $pages;

    public function init()
    {
        if($this->parent_id){
            $criteria = new CDbCriteria(array(
                'condition'=>'parent_id=:parent_id',
                'params'=>array(':parent_id'=>$this->parent_id),
            ));
            $criteria->addCondition("status = 1");
            $criteria->order = 't.order ASC';
            
            if($this->limit){
                $criteria->limit = $this->limit;
            }

            $this->pages = Page::model()->findAll($criteria);
        } elseif($this->id) {
            $this->pages = Page::model()->findByPk($this->id);
        }
        parent::init();
    }

    
    public function run()
    {
        $this->render($this->view,[
            'pages' => $this->pages,
            'butname' => $this->butname
        ]);
    }
}