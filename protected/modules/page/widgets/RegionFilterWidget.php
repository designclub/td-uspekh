<?php
/**
 * RegionFilterWidget виджет фильтрации областей
 *
 */
Yii::import('application.modules.page.models.*');

/**
 * Class PagesWidget
 */
class RegionFilterWidget extends yupe\widgets\YWidget
{
    /**
     * @var
     */
    public $parent_id;

    public $order = 't.order ASC, t.create_time ASC';

    public $limit;

    public $view = 'region-filter-widget';

    protected $regions;

    public function run()
    {
        $criteria = new CDbCriteria();
        $criteria->order = $this->order;
        $criteria->addCondition("parent_id = {$this->parent_id}");

        if ($this->limit) {
            $criteria->limit = (int)$this->limit;
        }

        $this->regions = Page::model()->findAll($criteria);

        $this->render(
            $this->view,
            [
                'regions' => $this->regions,
            ]
        );
    }
}
