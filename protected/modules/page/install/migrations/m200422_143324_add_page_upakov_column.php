<?php

class m200422_143324_add_page_upakov_column extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{page_page}}', 'upakov', 'text');
    }
    public function safeDown()
    {
       $this->addColumn('{{page_page}}', 'upakov');
    }
}