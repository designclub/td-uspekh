<?php

class m200423_143324_add_page_upakov_column extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{page_page}}', 'age', 'text');
        $this->addColumn('{{page_page}}', 'mass', 'text');
        $this->addColumn('{{page_page}}', 'uslovie', 'text');
        $this->addColumn('{{page_page}}', 'sertificat', 'text');
        $this->addColumn('{{page_page}}', 'desc2', 'text');
    }
    public function safeDown()
    {
       $this->addColumn('{{page_page}}', 'age');
       $this->addColumn('{{page_page}}', 'mass');
       $this->addColumn('{{page_page}}', 'uslovie');
       $this->addColumn('{{page_page}}', 'sertificat');
       $this->addColumn('{{page_page}}', 'desc2');
    }
}