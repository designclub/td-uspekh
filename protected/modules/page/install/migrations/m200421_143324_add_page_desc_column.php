<?php

class m200421_143324_add_page_desc_column extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{page_page}}', 'desc', 'text');
    }
    public function safeDown()
    {
       $this->addColumn('{{page_page}}', 'desc');
    }
}