<?php

class m200424_143324_add_page_big_image_column extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{page_page}}', 'imageBig', 'string');
    }
    public function safeDown()
    {
       $this->addColumn('{{page_page}}', 'imageBig', 'string');
    }
}